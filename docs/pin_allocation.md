## RPI GPIO

| _Odd pins description_     | _Pin name_  |     | _Pin name_  | _Even pin description_  |
| -------------------------- | ----------- | --- | ----------- | ----------------------- |
| Accelerometer VCC          | 3V3 (1)     |     | (2) 5V      | 5V power                |
| Accelerometer I2C SDA      | GPIO2 (3)   |     | (4) 5V      |                         |
| Accelerometer I2C SCL      | GPIO3 (5)   |     | (6) GND     | GND                     |
|                            | GPIO4 (7)   |     | (8) GPIO14  |                         |
|                            | GND (9)     |     | (10) GPIO15 |                         |
| MOTOR_LEFT_FORWARD         | GPIO17 (11) |     | (12) GPIO18 | MOTOR_RIGHT_FORWARD     |
| MOTOR_LEFT_BACKWARD        | GPIO27 (13) |     | (14) GND    |                         |
| ENCODER_LEFT               | GPIO22 (15) |     | (16) GPIO23 | MOTOR_RIGHT_BACKWARD    |
|                            | 3V3 (17)    |     | (18) GPIO24 | ENCODER_RIGHT           |
|                            | GPIO10 (19) |     | (20) GND    |                         |
|                            | GPIO9 (21)  |     | (22) GPIO25 |                         |
|                            | GPIO11 (23) |     | (24) GPIO8  |                         |
|                            | GND (25)    |     | (26) GPIO7  |                         |
|                            | GPIO0 (27)  |     | (28) GPIO1  |                         |
|                            | GPIO5 (29)  |     | (30) GND    |                         |
|                            | GPIO6 (31)  |     | (32) GPIO12 |                         |
| ULTRASOUND_TRIGGER_LEFT    | GPIO13 (33) |     | (34) GND    |                         |
| ULTRASOUND_TRIGGER_FORWARD | GPIO19 (35) |     | (36) GPIO16 | ULTRASOUND_ECHO_LEFT    |
| ULTRASOUND_TRIGGER_RIGHT   | GPIO26 (37) |     | (38) GPIO20 | ULTRASOUND_ECHO_FORWARD |
|                            | GND (39)    |     | (40) GPIO21 | ULTRASOUND_ECHO_RIGHT   |
