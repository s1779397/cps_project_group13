import numpy as np
# import matplotlib.pyplot as plt
from explore import Explore 

class StateMachine():
    def __init__(self, data_storage, driving_tracker):
        self.test = Explore(8, 5, data_storage=data_storage, driving_tracker=driving_tracker)
        self.lastState = 0
        self.state = 0
        self.leavingPossible = False

        
        # for xCord in range(0,200):
        #     self.test.borderForTesting(xCord,0)
        #     self.test.borderForTesting(xCord,199)
        # for yCord in range(0,200):
        #     self.test.borderForTesting(0,yCord)
        #     self.test.borderForTesting(199,yCord)
        # for yCord in range(70,111):
        #     self.test.borderForTesting(150,yCord)
        #     self.test.borderForTesting(160,yCord)
        # for xCord in range(150,161):
        #     self.test.borderForTesting(xCord,70)
        #     self.test.borderForTesting(xCord,110)
        self.test.updateMap()

    def changeStateManual(self, state):
        self.lastState = self.state
        self.state = state

    def run(self):
        if self.state == 0:
            self.test.stop()
        elif self.state == 1:
            # drive till first object detected
            if not self.test.detectBorderInFront():
                self.test.driveForward(1)
                self.test.updateMap()
            else:
                self.test.turnLeft(2)
                self.test.updateMap()
                # self.state = 2
                # self.lastState = 1

        elif self.state == 2:
            # drive along objects side, with object always on the right of the car
            # entry action, remember starting Point
            # return False
            noBorderInFront = not self.test.detectBorderInFront()
            borderRight = self.test.detectBorderRight()
            virtualBorderInFront = self.test.detectVirtualBorderInFront()

            if virtualBorderInFront and noBorderInFront:
                self.leavingPossible = True
            if  not self.lastState == 2:
                self.leavingPossible = False
                print("Entered State 2")
            if noBorderInFront:
                if borderRight:
                    # No Border in Front, Border to the right
                    self.test.driveForward(1)
                    self.test.updateMap()
                else:
                    # No Border in Front, No Border to the right
                    self.test.turnRight(2)
                    self.test.showHistoryMap()
                    self.test.showVirtualMap()
                    print("Turn Right")
                    print(self.leavingPossible)
            else:
                if borderRight:
                    # Border in Front and Border to the right
                    self.test.turnLeft(2)
                    self.test.showHistoryMap()
                    self.test.showVirtualMap()
                    print("Turn Left")
                else:
                    # Border to the front but not to the right
                    self.test.turnRight(2)

            #outro action, if again at starting point or no Virtual Border in Front anymore
            if (noBorderInFront and not virtualBorderInFront and self.leavingPossible) :
                self.state =3
                self.test.detectObject()
                self.test.updateMapFromObjectMap()
            
            self.lastState = 2
            self.test.updateMap()
            
        elif self.state == 3:
            # Driving towards Virtual Border Until No VirtualBorder detected or new Object detected
            borderInFront = self.test.detectBorderInFront()
            objectInFront = self.test.detectBorderInFront()
            # Entry action
            if not self.lastState == 3:
                print("Entered State 3")
            
            # Outro action
            if borderInFront and not objectInFront:
                # detected new object
                self.test.turnLeft(2)
                self.state = 2
                #toDo: Insert Object detection to the left and right
            
            else:
                if self.test.detectVirtualBorderInFront() and not borderInFront:
                    self.test.driveForward(1)
                elif self.test.detectVirtualBorderRight() and not self.test.detectBorderRight():
                    self.test.turnRight(2)
                    print("Turn Right")
                    self.test.showHistoryMap()
                    self.test.showVirtualMap()
                elif self.test.detectVirtualBorderLeft() and not self.test.detectBorderLeft():
                    self.test.turnLeft(2)
                    print("Turn Left")
                    self.test.showHistoryMap()
                    self.test.showVirtualMap()
                elif not self.test.detectVirtualBorderInFront() and not self.test.detectVirtualBorderLeft() and not self.test.detectVirtualBorderRight():
                    self.state = 4
                elif borderInFront and self.test.detectVirtualBorderInFront():
                    self.state = 2
                    self.test.turnLeft(2)
                else:

                    print(self.test.detectBorderInFront())
                    print(self.test.detectBorderLeft())
                    print(self.test.detectBorderRight())
                    print(self.test.detectVirtualBorderInFront())
                    print(self.test.detectVirtualBorderLeft())
                    print(self.test.detectVirtualBorderRight())
                    self.test.showVirtualMap()
                    self.test.showHistoryMap()
            self.test.updateMap()
            self.lastState = 3
            
        elif self.state == 4:
            # Drive to the middle Square
            print("Entered State 4")
            self.lastState = 4
            squareSizeObjectMap = int(self.test.map_size / 20)
            error = False
            
            targetX, targetY = self.test.getMiddleSquare(int(self.test.x/squareSizeObjectMap), int(self.test.y/squareSizeObjectMap))
            if not self.test.driveFromTo(self.test.x, self.test.y, targetX, self.test.y):
                # First drive straight to target X
                error = True
                self.state = 2
                self.test.turnLeft(2)
            if not self.test.driveFromTo(self.test.x, self.test.y, targetX, targetY) and not error:
                # Then drive straight to target Y
                error = True
                self.state = 2
                self.test.turnLeft(2)
            # drive along Path
            print("Drove to middle square")
            if not error:
                targetX, targetY = self.test.detectClosestVirtualBorder()
                if targetY == self.test.map_size and targetX == self.test.map_size:
                    # Exploring Finished
                    return False

                print(self.test.x, self.test.y, targetX, targetY)
                self.test.showVirtualMap()
                self.test.showHistoryMap()    
                path = self.test.getPath(self.test.x, self.test.y, targetX, targetY)
                print("got path:")

                path.reverse()
                print(path)
                for point in path:
                    tX,tY = self.test.getMiddleSquare(self.test.getxCord(point),self.test.getyCord(point))
                    print(self.test.x,self.test.y,tX,tY,self.test.getxCord(point),self.test.getyCord(point))
                    if not self.test.driveFromTo(self.test.x,self.test.y,tX,tY) and not error:
                        error = True
                        self.state = 2
                        self.test.turnLeft(2)
            print("drove along path")
            # drive to actual target
            if not self.test.driveFromTo(self.test.x, self.test.y, targetX, self.test.y) and not error:
                # First drive straight to target X
                error = True
                self.state = 2
                self.test.turnLeft(2)
            if not self.test.driveFromTo(self.test.x, self.test.y, targetX, targetY) and not error:
                # Then drive straight to target Y
                error = True
                self.state = 2
                self.test.turnLeft(2)
            print("drove to target")
            if not error:
                print("left state 4")
                self.state = 3

        return True
