# Libraries
import threading
import RPi.GPIO as GPIO
import time
from pathlib import Path
import numpy as np

# from mappingbot.constants import ULTRASOUND_ECHO_FORWARD, ULTRASOUND_ECHO_LEFT, ULTRASOUND_ECHO_RIGHT, \
#     ULTRASOUND_TRIGGER_FORWARD, ULTRASOUND_TRIGGER_LEFT, ULTRASOUND_TRIGGER_RIGHT, SONIC_SPEED
# from mappingbot.storage import DataStorage

from constants import ULTRASOUND_ECHO_FORWARD, ULTRASOUND_ECHO_LEFT, ULTRASOUND_ECHO_RIGHT, \
    ULTRASOUND_TRIGGER_FORWARD, ULTRASOUND_TRIGGER_LEFT, ULTRASOUND_TRIGGER_RIGHT, SONIC_SPEED
from storage import DataStorage


class UltrasoundGroup:

    def __init__(self, sensors: list, storage: DataStorage):
        """Initialize this ultrasound manager."""
        self.sensors = sensors
        self.storage = storage

    def start(self):
        """Start all sensor threads."""
        for sensor in self.sensors:
            sensor.start()

    def interrupt(self):
        for sensor in self.sensors:
            sensor.interrupt()

    def read_sensors(self) -> list:
        """Get the last recorded distance from all sensors."""
        result = list()
        for sensor in self.sensors:
            result.append(sensor.get_distance())
        return result

    def measure_and_store(self):
        """Get a measurement from all sensors and store that in the storage."""
        measurements = self.measure_sequentially()
        self.storage.add_data(time.time(), 0, 0, 0, *measurements)

    def measure_sequentially(self):
        """Pulse all sensors once sequentially. This hopefully prevents the sensors from detecting stray pulses from
        other sensors."""
        result = list()
        for sensor in self.sensors:
            sensor.measurement()
            result.append(sensor.get_distance())
        return result

    def print_sequentially(self):
        """Pulse all sensors once sequentially and print results. This hopefully prevents the sensors from detecting
        stray pulses from other sensors."""
        for sensor in self.sensors:
            sensor.measurement()
            print("Sensor %s: %0.1f cm" % (sensor.name, sensor.get_distance()))

    def print_distance_continuous(self, interval=1):
        """Print the distance continuously every interval seconds until the user interrupts it with ctrl-c."""
        try:
            while True:
                time.sleep(interval)
                self.print_sequentially()
        except KeyboardInterrupt:  # Exit by pressing CTRL + C
            print("Measurement stopped by User. Stopping sensors...")
            print("Sensors stopped.")


class UltrasoundSensor(threading.Thread):
    TRIGGER_DELAY = 10 / 1e6  # Time in s the trigger pin needs to be activated to send a pulse (default: 10 ms)
    MEASUREMENT_TIMEOUT = 0.2  # Time delay after which the sensor times out and stops its measurement

    def __init__(self, name, trigger_pin: int, echo_pin: int, polling_interval=0.1, daemon=True):
        """
        Set up this sensor. This is a threaded sensor, be sure to call .start().

        :param name: Name of the sensor.
        :param trigger_pin: Pin to which the trigger is connected
        :param echo_pin: Pin to which the echo is located
        :param polling_interval: Interval between measurements (if the thread is running)
        :param daemon: Whether to make this thread exit automatically when all other threads are stopped (recommended)
        """
        super().__init__(name=name, daemon=daemon)
        self.name = name
        self.trigger_pin = trigger_pin
        self.echo_pin = echo_pin
        self.polling_interval = polling_interval  # Interval between measurements

        self.distance_lock = threading.Lock()  # Lock to prevent thread race conditions in read/write to self._distance
        self.measurement_lock = threading.Lock()  # Lock to prevent multiple threads taking measurement at same time
        self._distance = 0  # Set initial distance to 0
        self.interrupted = False  # Flag to interrupt this thread

        GPIO.setup(self.trigger_pin, GPIO.OUT)  # Setup GPIO
        GPIO.setup(self.echo_pin, GPIO.IN)

    def measurement(self):
        """
        Get the distance last recorded by this sensor. The ECHO pin turns HIGH for the exact amount of time it took
        between sending and receiving the pulse. Therefore, we need to detect how long the ECHO pin is on exactly.
        """
        with self.measurement_lock:  # Make sure only one thread can take a measurement at a time
            GPIO.output(self.trigger_pin, GPIO.HIGH)  # set trigger to HIGH
            time.sleep(self.TRIGGER_DELAY)  # Keep trigger pin HIGH for 10 ms (time the sensor requires to send a pulse)
            GPIO.output(self.trigger_pin, GPIO.LOW)  # Set trigger to LOW again

            rising_time = time.time()  # Time at which echo pin turns from LOW to HIGH
            falling_time = time.time()  # Time at which echo pin turns from HIGH to LOW
            timeout_time = time.time() + self.MEASUREMENT_TIMEOUT  # Time at which the measurement times out and fails
            # save exact time at which the ECHO pin turns from LOW to HIGH. When this loop is exited, that is the exact time
            # echo pin turns HIGH
            while GPIO.input(self.echo_pin) == GPIO.LOW:
                rising_time = time.time()  # The pin is still low, update the recorded time
                if time.time() > timeout_time:  # Check if the measurement is timed out
                    raise TimeoutError("Measurement timed out.")

            # save exact time at which the ECHO pin turns from HIGH to LOW. When this loop is exited, that is the exact time
            # echo pin turns LOW
            while GPIO.input(self.echo_pin) == GPIO.HIGH:
                falling_time = time.time()  # The pin is still high, update the recorded time
                if time.time() > timeout_time:  # Check if the measurement is timed out
                    raise TimeoutError("Measurement timed out.")

            pulse_time = falling_time - rising_time  # time difference between rising and falling edge of echo pin
            distance = (pulse_time * SONIC_SPEED) / 2  # Convert time to distance
            with self.distance_lock:  # Use the lock to store the distance internally without race conditions between threads
                self._distance = distance
            return distance  # Return the recorded distance

    def run(self):
        GPIO.setmode(GPIO.BCM)
        """Overridden method for the running thread."""
        while self.interrupted is not True:
            self.measurement()  # Start a measurement
            time.sleep(self.polling_interval)

    def interrupt(self):
        self.interrupted = True
        self.join()

    def get_distance(self) -> float:
        """Get the distance last recorded by this sensor in meters."""
        with self.distance_lock:  # Only one thread may access self._distance at a time
            return self._distance


class _UltrasoundSensorInterrupt(threading.Thread):
    """
    NOTE: This class is still in here in case we need the interrupt method at some point but for now it is not to be
    used.
    """
    TRIGGER_DELAY = 10 / 1e6  # Time in s the trigger pin needs to be activated to send a pulse (default: 10 ms)

    def __init__(self, name, trigger_pin: int, echo_pin: int, polling_interval=0.5, daemon=True):
        """
        Set up this sensor. This is a threaded sensor, be sure to call .start().

        :param name: Name of the sensor.
        :param trigger_pin: Pin to which the trigger is connected
        :param echo_pin: Pin to which the echo is located
        :param polling_interval: Interval between measurements (if the thread is running)
        :param daemon: Whether to make this thread exit automatically when all other threads are stopped (recommended)
        """
        super().__init__(name=name, daemon=daemon)
        self.name = name
        self.trigger_pin = trigger_pin
        self.echo_pin = echo_pin
        self.polling_interval = polling_interval  # Interval between measurements

        self.lock = threading.RLock()  # Lock object to prevent threads from interfering
        self._distance = 0  # Set initial distance to 0
        self.interrupted = False  # Flag to interrupt this thread
        self.measurement_done = threading.Event()  # Event which indicates a measurement is done

        GPIO.setup(self.trigger_pin, GPIO.OUT)  # Setup GPIO
        GPIO.setup(self.echo_pin, GPIO.IN)

    def run(self):
        """Overridden method for the running thread."""
        while self.interrupted is not True:
            self.start_measurement()  # Start a measurement
            time.sleep(self.polling_interval)

    def start_measurement(self):
        """
        Get the distance last recorded by this sensor. The ECHO pin turns HIGH for the exact amount of time it took
        between sending and receiving the pulse. Therefore, we need to detect how long the ECHO pin is on exactly.
        """

        def _pin_read(channel):
            """Callback function for echo pin RISING edge detection (low to high)."""
            nonlocal pulse_rising, pulse_falling  # Get the variable from the one scope above
            if channel != self.echo_pin:
                print("Warning: echo pin received from channel %d, expected %d" % (channel, self.echo_pin))

            with self.lock:
                if GPIO.input(self.echo_pin) == GPIO.HIGH:  # Rising edge detected
                    pulse_rising = time.time()
                else:  # Falling edge detected
                    pulse_falling = time.time()
                    if pulse_falling == 0:
                        print("Warning: falling edge was detected before rising edge on channel %d!" % self.echo_pin)
                    time_elapsed = pulse_falling - pulse_rising  # time difference between sending and receiving pulse
                    self._distance = time_elapsed * SONIC_SPEED / 2  # We divide by two because of the reflection of the wave
                    GPIO.remove_event_detect(self.echo_pin)
                    self.measurement_done.set()  # Measurement is finished

        with self.lock:  # Only one thread may use the sensor at a time
            self.measurement_done.clear()  # Let any waiting threads know there is currently a measurement running
            pulse_rising = 0  # Set initial time for rising pulse to 0
            pulse_falling = 0  # Idem
            GPIO.add_event_detect(self.echo_pin, GPIO.BOTH, _pin_read)  # Register the callback for rising edge
            # GPIO.add_event_detect(self.echo_pin, GPIO.FALLING, _echo_pin_falling)  # Idem for falling edge
            GPIO.output(self.trigger_pin, GPIO.HIGH)  # Fire ultrasound pulse
            time.sleep(self.TRIGGER_DELAY)  # Wait for at least 10 us (delay required for the sensor to fire)
            GPIO.output(self.trigger_pin, GPIO.LOW)  # Stop firing the pulse

    def get_distance(self):
        """Get the distance last recorded by this sensor."""
        with self.lock:  # Only one thread may use the sensor at a time
            return self._distance

    def print_distance(self, unit='cm') -> None:
        if unit == 'm':
            dist = self.get_distance()
            print("%12s measured %0.3f m" % (self.name, dist))
        else:  # Unit is cm or not recognised
            dist = self.get_distance() * 100.0
            print("%12s measured %0.1f cm" % (self.name, dist))


if __name__ == '__main__':
    try:
        storage = DataStorage()

        # GPIO Mode (BOARD / BCM)
        GPIO.setmode(GPIO.BCM)

        sensor1 = UltrasoundSensor(name="us_left", trigger_pin=ULTRASOUND_TRIGGER_LEFT, echo_pin=ULTRASOUND_ECHO_LEFT)
        sensor2 = UltrasoundSensor(name="us_fwd", trigger_pin=ULTRASOUND_TRIGGER_FORWARD,
                                   echo_pin=ULTRASOUND_ECHO_FORWARD)
        sensor3 = UltrasoundSensor(name="us_right", trigger_pin=ULTRASOUND_TRIGGER_RIGHT,
                                   echo_pin=ULTRASOUND_ECHO_RIGHT)

        manager = UltrasoundGroup([sensor1, sensor2, sensor3], storage)
        stop_time = time.time() + 6
        while time.time() < stop_time:  # Until 10 seconds passed
            manager.measure_and_store()
            time.sleep(1)
        x = storage.to_array()
        np.set_printoptions(suppress=True, formatter={'float_kind': '{:0.3f}'.format})
        print(x)

    finally:  # Always clean up the pins after you are done with them
        GPIO.cleanup()
        print("Cleaned up.")
