import numpy as np
# import matplotlib.pyplot as plt
from explore import Explore 

class StateMachine():
    def __init__(self, data_storage, driving_tracker):
        self.explore = Explore(8, 5, data_storage=data_storage, driving_tracker=driving_tracker)
        self.lastState = 0
        self.state = "followWall"
        self.leavingPossible = False
        self.targetDistanceWall = 0
        self.detectedNothingRight = False
        self.previousRightDistance = 0
        self.explore.updateMap()

    def changeStateManual(self, state):
        self.lastState = self.state
        self.state = state

    def run(self):
        if self.state == "followWall":
            # The mappingBot will follow a right wall
            self.explore.car.resetOrientation()
            if self.targetDistanceWall == 0:
                # set target distance to wall. The car will try to keep this distance.
                self.targetDistanceWall = self.explore.distanceWallRight()
                self.previousRightDistance = self.targetDistanceWall  # Initial value
                print("target: " + str(self.targetDistanceWall))

            if not self.explore.detectObstacle() and self.explore.detectWallRight() and \
                    abs(self.previousRightDistance - self.explore.distanceWallRight()) < 10:
                # Keep driving forward

                # Compensate when distance to the wall changes
                if self.targetDistanceWall - self.explore.distanceWallRight() > 2:
                    if self.targetDistanceWall - self.explore.distanceWallRight() > 5:
                        self.explore.car.changePWMRight(18)
                    elif self.explore.distanceWallRight() < 5:
                        self.explore.car.changePWMRight(25)
                    else:
                        self.explore.car.changePWMRight(11)
                elif self.targetDistanceWall - self.explore.distanceWallRight() < -2:
                    if self.targetDistanceWall - self.explore.distanceWallRight() < -5:
                        self.explore.car.changePWMLeft(18)
                    else:
                        self.explore.car.changePWMLeft(11)

                self.explore.driveForward(1)
            elif abs(self.previousRightDistance - self.explore.distanceWallRight()) > 10:
                # Right wall is gone, turn right
                for i in range(0, 5):
                    # Drive forward to clear the corner
                    if not self.explore.detectObstacle():
                        self.explore.driveForward(1)
                self.targetDistanceWall = 0
                self.explore.turnRight(2)
                while not self.explore.detectObstacle() and not self.explore.detectWallRight():
                    # Drive forward until around corner
                    self.explore.driveForward(1)
                    self.explore.updateMap()
            elif not self.explore.detectWallLeft():
                # Can't drive forward or turn right, then turn left
                self.targetDistanceWall = 0
                self.explore.turnLeft(2)
            self.previousRightDistance = self.explore.distanceWallRight()
            self.explore.updateMap()
        elif self.state == 0:
            self.explore.stop()
        # This used to have more states, but those were scrapped
        # These stated can still be found in "virtual_mapping copy.py"
        return True
