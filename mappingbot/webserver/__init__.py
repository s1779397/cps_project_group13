import os
from pathlib import Path
from threading import Thread

from flask import Flask


def create_app(test_config=None, explorer=None):
    webserver_dir = Path(__file__).parent  # Return the package folder
    instance_dir = Path.joinpath(webserver_dir, 'instance')  # Add the instance folder

    # create and configure the app
    app = Flask(__name__, instance_path=instance_dir, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='mappingbot',
    )

    app.config['EXPLORER_OBJECT'] = explorer

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # Register all blueprints
    # from mappingbot.webserver.routes import register_all_blueprints
    from webserver.routes import register_all_blueprints
    register_all_blueprints(app)

    return app


class WebserverThread(Thread):

    def __init__(self, host=None, port: int = None, debug=None, load_dotenv: bool = False, test_config=None,
                 explorer=None):
        """
        Create a threaded flask webserver.

        :param host: Hostname (default: localhost)
        :param port: Port nr (default: 5000)
        :param debug: Debugging mode (default: False)
        :param load_dotenv: Load from .flaskenv files (default: False)
        :param test_config: Pass a test config (default: None)
        :param explorer: Explorer data tracking object (default: None)
        """
        super().__init__(daemon=True)
        self.app = create_app(test_config=test_config, explorer=explorer)
        self.host = host
        self.port = port
        self.debug = debug
        self.load_dotenv = load_dotenv
        self.explorer = explorer

    def run(self):
        self.app.run(self.host, self.port, self.debug, self.load_dotenv)


if __name__ == '__main__':
    webserver = WebserverThread(host='0.0.0.0', debug=True)
    webserver.run()  # NOTE: Use .start() instead of .run() to make it run as a thread (recommended)
