from flask import Blueprint, render_template, current_app, url_for

from PIL import Image
import numpy as np
import sys
from matplotlib import image

# from mappingbot.constants import DATA_FOLDER_PATH, FILENAME_MAPPINGDATA, FILENAME_BLUETOOTHMAP, FILENAME_DATAMAP, \
#     FILENAME_HISTORYMAP, FILENAME_VIRTUALMAP
from constants import DATA_FOLDER_PATH, FILENAME_MAPPINGDATA, FILENAME_BLUETOOTHMAP, FILENAME_DATAMAP, \
    FILENAME_HISTORYMAP, FILENAME_VIRTUALMAP


def register_all_blueprints(app):
    """Registers all blueprints."""
    app.register_blueprint(bp_main)
    app.register_blueprint(bp_data)
    app.register_blueprint(bp_view)
    app.register_blueprint(bp_raw)


def _rescale_linear(array, new_min, new_max):
    """Rescale an arrary linearly.
    Source: https://stackoverflow.com/questions/36000843/scale-numpy-array-to-certain-range"""
    minimum, maximum = np.min(array), np.max(array)
    m = (new_max - new_min) / (maximum - minimum)
    b = new_min - m * minimum
    return m * array + b


bp_main = Blueprint('main', __name__)


@bp_main.route('/')
@bp_main.route('/index')
@bp_main.route('/home')
def index():
    """A simple index page."""
    return render_template('index.html')


@bp_main.route('/hello')
def hello():
    """A simple page that says hello."""
    return 'Hello, World!'


# ----- Visualizing data in greyscale images -----
bp_view = Blueprint('view', __name__, url_prefix='/view')


@bp_view.route('/')
@bp_view.route('/overview')
def overview():
    datamap_file = datamap()
    mappingdata_file = ''  # mappingdata_filename = mappingdata()  # Mappingdata is disabled
    bluetoothmap_file = bluetoothmap()
    historymap_file = historymap()
    virtualmap_file = virtualmap()
    return render_template('view.html', datamap_file=datamap_file, mappingdata_file=mappingdata_file,
                           bluetoothmap_file=bluetoothmap_file, historymap_file=historymap_file,
                           virtualmap_file=virtualmap_file)


@bp_view.route('/datamap')
def datamap(random=None):
    if current_app.config['EXPLORER_OBJECT'] is not None:
        current_app.config['EXPLORER_OBJECT'].save_data_map()
    data = np.loadtxt(DATA_FOLDER_PATH / FILENAME_DATAMAP, delimiter='  ', )
    data = _rescale_linear(data, 0, 255)  # Rescale the array to greyscale pixels (0,255)
    data = 255 - data  # Invert the pixels (high value -> darker)
    data = data.astype(np.uint8)

    image = Image.fromarray(data)
    image = image.convert("L")
    # image = image.resize((400, 400), resample=Image.BOX)
    image.save(DATA_FOLDER_PATH / 'datamap.jpeg')
    image.save(current_app.static_folder + '/datamap.jpeg')
    return url_for('static', filename='datamap.jpeg')


@bp_view.route('/bluetoothmap')
def bluetoothmap(random=None):
    if current_app.config['EXPLORER_OBJECT'] is not None:
        current_app.config['EXPLORER_OBJECT'].save_bluetooth_map()  # TODO: Look if this exists
    data = np.loadtxt(DATA_FOLDER_PATH / FILENAME_BLUETOOTHMAP, delimiter='  ', )
    data = _rescale_linear(data, 0, 255)  # Rescale the array to greyscale pixels (0,255)
    data = 255 - data  # Invert the pixels (high value -> darker)
    data = data.astype(np.uint8)

    image = Image.fromarray(data)
    image = image.convert("L")
    # image = image.resize((400, 400), resample=Image.BOX)
    image.save(DATA_FOLDER_PATH / 'bluetoothmap.jpeg')
    image.save(current_app.static_folder + '/bluetoothmap.jpeg')
    return url_for('static', filename='bluetoothmap.jpeg')


@bp_view.route('/historymap')
def historymap(random=None):
    if current_app.config['EXPLORER_OBJECT'] is not None:
        current_app.config['EXPLORER_OBJECT'].save_history_map()
    data = np.loadtxt(DATA_FOLDER_PATH / FILENAME_HISTORYMAP, delimiter='  ', )
    data = _rescale_linear(data, 0, 255)  # Rescale the array to greyscale pixels (0,255)
    data = 255 - data  # Invert the pixels (high value -> darker)
    data = data.astype(np.uint8)

    image = Image.fromarray(data)
    image = image.convert("L")
    # image = image.resize((400, 400), resample=Image.BOX)
    image.save(DATA_FOLDER_PATH / 'historymap.jpeg')
    image.save(current_app.static_folder + '/historymap.jpeg')
    return url_for('static', filename='historymap.jpeg')


@bp_view.route('/virtualmap')
def virtualmap(random=None):
    if current_app.config['EXPLORER_OBJECT'] is not None:
        current_app.config['EXPLORER_OBJECT'].save_virtual_map()
    data = np.loadtxt(DATA_FOLDER_PATH / FILENAME_VIRTUALMAP, delimiter='   ', )
    data = _rescale_linear(data, 0, 255)  # Rescale the array to greyscale pixels (0,255)
    data = 255 - data  # Invert the pixels (high value -> darker)
    data = data.astype(np.uint8)

    image = Image.fromarray(data)
    image = image.convert("L")
    # image = image.resize((400, 400), resample=Image.BOX)
    image.save(DATA_FOLDER_PATH / 'virtualmap.jpeg')
    image.save(current_app.static_folder + '/virtualmap.jpeg')
    return url_for('static', filename='virtualmap.jpeg')

# ----- Displaying raw data -----
bp_raw = Blueprint('raw', __name__, url_prefix='/raw')


@bp_raw.route('/mappingdata')
def raw_mappingdata():
    with open(DATA_FOLDER_PATH / FILENAME_MAPPINGDATA) as file:
        lines = file.read()
        return lines


@bp_raw.route('/virtualmap')
def raw_virtualmap():
    with open(DATA_FOLDER_PATH / FILENAME_VIRTUALMAP) as file:
        lines = file.read()
        return lines


@bp_raw.route('/datamap')
def raw_datamap():
    with open(DATA_FOLDER_PATH / FILENAME_DATAMAP) as file:
        lines = file.read()
        return lines


@bp_raw.route('/historymap')
def raw_historymap():
    with open(DATA_FOLDER_PATH / FILENAME_HISTORYMAP) as file:
        lines = file.read()
        return lines


@bp_raw.route('/bluetoothmap')
def raw_bluetoothmap():
    with open(DATA_FOLDER_PATH / FILENAME_BLUETOOTHMAP) as file:
        lines = file.read()
        return lines


# ----- Displaying data in table -----
bp_data = Blueprint('data', __name__, url_prefix='/data')
#
#
# @bp_data.route('/mappingdata')
# def mappingdata():
#     data = np.genfromtxt(DATA_FOLDER_PATH / FILENAME_MAPPINGDATA, delimiter=',')
#     result = np.array2string(data, separator=', ')
#     return result
#
#
# @bp_data.route('/datamap')
# def datamap():
#     data = np.loadtxt(DATA_FOLDER_PATH / FILENAME_DATAMAP, delimiter='  ', )
#     with np.printoptions(threshold=sys.maxsize):
#         result = np.array2string(data, separator=', ')
#     return result
