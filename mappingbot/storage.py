import sys

import numpy as np
from pathlib import Path
import threading
import time

# from mappingbot.constants import DATA_FOLDER


from constants import DATA_FOLDER, FILENAME_MAPPINGDATA


class DataStorage:

    def __init__(self, lock=None):
        """
        Initialize a new storage object which holds the data points for all ultrasound sensors. It also allows the data
        to be stored to the disk.
        """
        self._data = list()  # Initialize empty list which holds the datapoints
        if lock is None:
            self.lock = threading.RLock()
        else:
            self.lock = lock

    def get_nr_rows(self):
        """Get the number of data points in the currently stored data."""
        with self.lock:
            return len(self._data)

    def get_nr_cols(self):
        """Get the number of columns of the currently stored data (headers)."""
        with self.lock:
            if self.get_nr_rows() != 0:
                return len(self._data[0])
            else:
                return 0

    def add_data_row(self, *args):
        """
        Add a datapoint to the currently stored data. If there is already data present, the number of data points
        provided should match the number of columns.
        """
        if self.get_nr_rows() != 0 and len(args) != self.get_nr_cols():
            raise ValueError(
                "Wrong number of parameters provided. Expected %d, got %d. Current data is %d by %d in size." % (
                    self.get_nr_cols(), len(args), self.get_nr_rows(), self.get_nr_cols()))
        new_data_line = list()
        for arg in args:
            new_data_line.append(arg)

        with self.lock:
            self._data.append(new_data_line)

    def add_data(self, timestamp: float = 0, x: float = 0, y: float = 0, direction: float = 0, sensor1: float = 0,
                 sensor2: float = 0, sensor3: float = 0, x_robot: float = 0, y_robot: float = 0, angle: float = 0):
        """Store data in predefined header columns (this forces you to store data in a specific way)."""
        self.add_data_row(timestamp, x, y, direction, sensor1, sensor2, sensor3, x_robot, y_robot, angle)

    def save_to_disk(self, filename: str = FILENAME_MAPPINGDATA, output_dir=None):
        """Save the data to the disk in the specified format."""
        data_array = self.to_array()
        if output_dir is None:  # If no other path is given, use the data folder
            output_dir = Path(__file__).parents[1] / DATA_FOLDER

        try:
            extension = filename.split('.')[-1]  # Get the last element after a dot (ususally extension)
        except IndexError:
            raise ValueError("No proper filename extension detected. Please add an extension (e.g. '.txt.' or '.csv').")

        filepath = output_dir / filename

        if extension == 'txt':
            np.savetxt(filepath, data_array, fmt='%.3e')
        elif extension == 'csv':
            np.savetxt(filepath, data_array, delimiter='    ', fmt='%f')
        elif extension == 'npy':
            np.save(filepath, data_array)
        else:
            raise ValueError("Extension '.%s' not supported." % extension)

    def to_array(self):
        """Return the currently stored data as a numpy array."""
        with self.lock:
            return np.asarray(self._data)


class DataCollector(threading.Thread):
    NAME = 'DataCollector'
    DEFAULT_INTERVAL = 1

    def __init__(self, data_storage: DataStorage, driving_tracker, ultrasound_group, explore_tracker, interval=DEFAULT_INTERVAL):
        """
        Initialize a DataCollector thread object.

        :param data_storage:
        :param driving_tracker:
        :param ultrasound_group:
        :param interval:
        """
        super().__init__(name=self.NAME)
        self.data_storage = data_storage
        self.driving_tracker = driving_tracker
        self.ultrasound_group = ultrasound_group
        self.explore_tracker = explore_tracker
        self.interval = interval

        self.interrupted = False

    def run(self):
        """Main method of this thread. Use .start() to start this."""
        while self.interrupted is False:
            timestamp = time.time()
            x_robot, y_robot, angle = self.driving_tracker.returnPosition()
            x, y, direction = self.explore_tracker.returnPosition()
            distances = self.ultrasound_group.read_sensors()  # get the list of distances from the ultrasound sensors

            # Add data to the data storage. We use *distances to split out the list into different entries
            self.data_storage.add_data(timestamp, x, y, direction, *distances, x_robot, y_robot, angle)
            np.set_printoptions(precision=1)

            time.sleep(self.interval)

    def interrupt(self):
        """Stop this thread and joint it."""
        self.interrupted = True
        self.join()
