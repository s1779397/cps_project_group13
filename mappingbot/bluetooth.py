from bluepy.btle import Scanner

class DistanceBLE:
    def __init__(self):
        self.scanner = Scanner()
        self.values = []
        self.oldAverage = 0
        self.average = 0

    def run(self):
        devices = self.scanner.scan(0.3)  # lower delay will result in more iterations without return value
        for device in devices:
            if device.addr == "30:4b:07:ff:e0:3e":  # mac address of BLE source
                # Calculate rolling average
                self.values.append(device.rssi)
                if len(self.values) > 5:
                    self.values.pop(0)
                valSum = sum(self.values)
                self.oldAverage = self.average
                self.average = valSum/(len(self.values))

    def getAverageRSSI(self):
        # return average RSSI
        return self.average


if __name__ == '__main__':
    try:
        ble = DistanceBLE()
        ble.run()

    except KeyboardInterrupt:
        print("Measurement stopped by User")
    finally:
        print("exit")

