import numpy as np
import matplotlib.pyplot as plt
from explore import Explore 
from state_machine_VirtualMapping import StateMachine

machine = StateMachine()
machine.changeStateManual(1)
condition = True
while condition :
    condition = machine.run()


targetX ,targetY =machine.test.detectClosestVirtualBorder()
#print(machine.test.getPath(machine.test.x,machine.test.y,targetX , targetY ).pop())

machine.test.showVirtualMap()
machine.test.showHistoryMap()
