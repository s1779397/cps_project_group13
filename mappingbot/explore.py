from driving import Driving
import RPi.GPIO as GPIO
import time
import numpy as np
from pathlib import Path
from constants import DATA_FOLDER
from bluetooth import DistanceBLE
# import matplotlib.pyplot as plt


# GPIO.setmode(GPIO.BCM)
# print(GPIO.VERSION)
# from mappingbot.constants import DATA_FOLDER


class Explore:

    def __init__(self, bubble_size, resolutuion, data_storage, driving_tracker):
        self.data_storage = data_storage  # Store data storage object which will hold all the sensor and position data

        self.bubble_size = bubble_size  # unit is the resolution
        self.carWidth = 5  # unit is the resolution
        self.x = 100
        self.y = 100

        self.orientation = 0  # 0 = North, 180 = South, 90 = east, 270 = west
        self.resolutuion = resolutuion  # size of square for one coordinate
        self.map_size = 200  # needs to be a multiple of 20
        self.dataMap = np.zeros((self.map_size, self.map_size))  # number of datapoints detected
        # This data Map stores how many datapoints are detected at each coordinate

        self.Virtualmap = np.zeros((self.map_size, self.map_size))
        # 0 = undiscovered, 1 = virtual boarder, 2 = dicovered, 3= actual boarder, 4=object in this area

        self.historyMap = np.zeros((self.map_size, self.map_size))
        # 0 = point not visited, 1= point visited
        self.objectMap = np.zeros((20, 20))
        # 0 = no object in area # 1 object in area
        # This Map has a worse resolution than the Virtual Map.
        # Its purpuse is to store in which areas there are detected objects or walls
        # It is used to be the source for generating a graph for path finding algorithms

        self.BluetoothMap = np.zeros((self.map_size, self.map_size))

        self.numberOfEvaluatedPoints = 0
        self.thresholdBoarder = 2
        # This many dataPoints in one Square are needed to declare as border
        self.thresholdObject = 0.1
        # Percentage of Coordinates that need to be borders in order to declare a object in a area

        self.DistanceBluetooth = DistanceBLE()
        self.car = driving_tracker
        self.car.initXY(100*resolutuion, 100*resolutuion)
        self.forwardPWM = 50

    def detectBorderInFront(self):
        # print(self.dataMap)
        # checks if there is a real Border in Front.
        # Depth of Search = Bubble Size
        # Width of Searh = half of the Bubble Size
        centerOfSearchX = self.x
        centerOfSearchY = self.y
        radius = int(self.bubble_size / 4)
        detected = False

        # Calculate CenterOfSearch Depending on orientation
        if self.orientation == 0:
            centerOfSearchY += (radius)
        elif self.orientation == 90:
            centerOfSearchX += (radius)
        elif self.orientation == 180:
            centerOfSearchY -= (radius)
        elif self.orientation == 270:
            centerOfSearchX -= (radius)

        for xCord in range(centerOfSearchX - radius, centerOfSearchX + radius + 1):
            for yCord in range(centerOfSearchY - radius, centerOfSearchY + radius + 1):
                if xCord > self.map_size - 1 or yCord > self.map_size - 1 or xCord < 0 or yCord < 0:
                    print("Search out of range" + str(xCord) + "   " + str(yCord))
                elif self.Virtualmap[xCord][yCord] == 3 or self.Virtualmap[xCord][yCord] == 4:
                    detected = True
                # self.Virtualmap[xCord][yCord]= 4
        print(detected)
        return detected

    def detectObstacle(self):
        dataArray = self.data_storage.to_array()
        #print("front: " + str(dataArray[self.numberOfEvaluatedPoints - 1][5]))
        if dataArray[self.numberOfEvaluatedPoints-1][5] < 30:
            return True
        else:
            return False

    def detectWallLeft(self):
        dataArray = self.data_storage.to_array()
        #print("left: " + str(dataArray[self.numberOfEvaluatedPoints - 1][4]))
        if dataArray[self.numberOfEvaluatedPoints - 1][4] < 30:
            return True
        else:
            return False

    def detectWallRight(self):
        dataArray = self.data_storage.to_array()
        #print("right: " + str(dataArray[self.numberOfEvaluatedPoints - 1][6]))
        if dataArray[self.numberOfEvaluatedPoints - 1][6] < 60:
            return True
        else:
            return False

    def distanceWallRight(self) -> float:
        dataArray = self.data_storage.to_array()
        return dataArray[self.numberOfEvaluatedPoints - 1][6]

    def detectObjectInFront(self):
        # checks if there is a real Border in Front.
        # Depth of Search = Bubble Size
        # Width of Searh = half of the Bubble Size
        centerOfSearchX = self.x
        centerOfSearchY = self.y
        radius = int(self.bubble_size / 2)
        detected = False

        # Calculate CenterOfSearch Depending on orientation
        if self.orientation == 0:
            centerOfSearchY += (radius)
        elif self.orientation == 90:
            centerOfSearchX += (radius)
        elif self.orientation == 180:
            centerOfSearchY -= (radius)
        elif self.orientation == 270:
            centerOfSearchX -= (radius)

        for xCord in range(centerOfSearchX - radius, centerOfSearchX + radius + 1):
            for yCord in range(centerOfSearchY - radius, centerOfSearchY + radius + 1):
                if xCord > self.map_size - 1 or yCord > self.map_size - 1 or xCord < 0 or yCord < 0:
                    print("Search ot of range")
                elif self.Virtualmap[xCord][yCord] == 4:
                    detected = True
                # self.Virtualmap[xCord][yCord]= 4
        return detected

    def detectBorderLeft(self):
        self.virtualTurnLeft(2)
        # checks if there is a real Border in Front.
        # Depth of Search = Bubble Size
        # Width of Searh = half of the Bubble Size
        centerOfSearchX = self.x
        centerOfSearchY = self.y
        radius = int(self.bubble_size / 2)
        detected = False

        # Calculate CenterOfSearch Depending on orientation
        if self.orientation == 0:
            centerOfSearchY += (radius)
        elif self.orientation == 90:
            centerOfSearchX += (radius)
        elif self.orientation == 180:
            centerOfSearchY -= (radius)
        elif self.orientation == 270:
            centerOfSearchX -= (radius)

        for xCord in range(centerOfSearchX - radius, centerOfSearchX + radius + 1):
            for yCord in range(centerOfSearchY - radius, centerOfSearchY + radius + 1):
                if xCord > self.map_size - 1 or yCord > self.map_size - 1 or xCord < 0 or yCord < 0:
                    print("Search ot of range")
                elif self.Virtualmap[xCord][yCord] == 3 or self.Virtualmap[xCord][yCord] == 4:
                    detected = True
                # self.Virtualmap[xCord][yCord]= 4
        self.virtualTurnRight(2)
        return detected

    def detectBorderRight(self):
        self.virtualTurnRight(2)
        # checks if there is a real Border in Front.
        # Depth of Search = Bubble Size
        # Width of Searh = half of the Bubble Size
        centerOfSearchX = self.x
        centerOfSearchY = self.y
        radius = int(self.bubble_size / 2)
        detected = False

        # Calculate CenterOfSearch Depending on orientation
        if self.orientation == 0:
            centerOfSearchY += (radius)
        elif self.orientation == 90:
            centerOfSearchX += (radius)
        elif self.orientation == 180:
            centerOfSearchY -= (radius)
        elif self.orientation == 270:
            centerOfSearchX -= (radius)

        for xCord in range(centerOfSearchX - radius, centerOfSearchX + radius + 1):
            for yCord in range(centerOfSearchY - radius, centerOfSearchY + radius + 1):
                if xCord > self.map_size - 1 or yCord > self.map_size - 1 or xCord < 0 or yCord < 0:
                    print("Search ot of range")
                elif self.Virtualmap[xCord][yCord] == 3 or self.Virtualmap[xCord][yCord] == 4:
                    detected = True
                # self.Virtualmap[xCord][yCord]= 4
        self.virtualTurnLeft(2)
        return detected

    def detectVirtualBorderInFront(self):
        # checks if there is a real Border in Front.
        # Depth of Search = Bubble Size
        # Width of Searh = half of the Bubble Size
        centerOfSearchX = self.x
        centerOfSearchY = self.y
        radius = int(self.bubble_size / 2)
        detected = False

        # Calculate CenterOfSearch Depending on orientation
        if self.orientation == 0:
            centerOfSearchY += (radius)
        elif self.orientation == 90:
            centerOfSearchX += (radius)
        elif self.orientation == 180:
            centerOfSearchY -= (radius)
        elif self.orientation == 270:
            centerOfSearchX -= (radius)

        for xCord in range(centerOfSearchX - radius, centerOfSearchX + radius + 1):
            for yCord in range(centerOfSearchY - radius, centerOfSearchY + radius + 1):
                if xCord > self.map_size - 1 or yCord > self.map_size - 1 or xCord < 0 or yCord < 0:
                    print("Search ot of range")
                elif self.Virtualmap[xCord][yCord] == 1:
                    detected = True
                # self.Virtualmap[xCord][yCord]= 4
        return detected

    def detectVirtualBorderLeft(self):
        # checks if there is a real Border in Left.
        # Depth of Search = Bubble Size
        # Width of Searh = half of the Bubble Size
        self.virtualTurnLeft(2)
        centerOfSearchX = self.x
        centerOfSearchY = self.y
        radius = int(self.bubble_size / 2)
        detected = False

        # Calculate CenterOfSearch Depending on orientation
        if self.orientation == 0:
            centerOfSearchY += (radius)
        elif self.orientation == 90:
            centerOfSearchX += (radius)
        elif self.orientation == 180:
            centerOfSearchY -= (radius)
        elif self.orientation == 270:
            centerOfSearchX -= (radius)

        for xCord in range(centerOfSearchX - radius, centerOfSearchX + radius + 1):
            for yCord in range(centerOfSearchY - radius, centerOfSearchY + radius + 1):
                if xCord > self.map_size - 1 or yCord > self.map_size - 1 or xCord < 0 or yCord < 0:
                    print("Search ot of range")
                elif self.Virtualmap[xCord][yCord] == 1:
                    detected = True
                # self.Virtualmap[xCord][yCord]= 4
        self.virtualTurnRight(2)
        return detected

    def detectVirtualBorderRight(self):
        # checks if there is a real Border in Left.
        # Depth of Search = Bubble Size
        # Width of Searh = half of the Bubble Size
        self.virtualTurnRight(2)
        centerOfSearchX = self.x
        centerOfSearchY = self.y
        radius = int(self.bubble_size / 2)
        detected = False

        # Calculate CenterOfSearch Depending on orientation
        if self.orientation == 0:
            centerOfSearchY += (radius)
        elif self.orientation == 90:
            centerOfSearchX += (radius)
        elif self.orientation == 180:
            centerOfSearchY -= (radius)
        elif self.orientation == 270:
            centerOfSearchX -= (radius)

        for xCord in range(centerOfSearchX - radius, centerOfSearchX + radius + 1):
            for yCord in range(centerOfSearchY - radius, centerOfSearchY + radius + 1):
                if xCord > self.map_size - 1 or yCord > self.map_size - 1 or xCord < 0 or yCord < 0:
                    print("Search ot of range")
                elif self.Virtualmap[xCord][yCord] == 1:
                    detected = True
                # self.Virtualmap[xCord][yCord]= 4
        self.virtualTurnLeft(2)
        return detected

    def detectClosestVirtualBorder(self):
        for distance in range(1, self.map_size):

            for xCord in range(self.x - distance, self.x + distance + 1):
                for yCord in range(self.y - distance, self.y + distance + 1):
                    if not (xCord > self.map_size - 1 or yCord > self.map_size - 1 or xCord < 0 or yCord < 0):
                        if self.Virtualmap[xCord][yCord] == 1:
                            return xCord, yCord

        return self.map_size, self.map_size  # This means that the discovering is finished

    def borderForTesting(self, xCord, yCord):
        self.Virtualmap[xCord][yCord] = 3

    def driveForward(self, steps):
        #print(self.forwardPWM)
        # steps: number of virual squares to drive forward
        # actually drive car
        self.car.driveForward(steps*self.resolutuion, self.forwardPWM)
        # update own position
        if self.orientation == 0:
            self.y += steps
        elif self.orientation == 90:
            self.x += steps
        elif self.orientation == 180:
            self.y -= steps
        elif self.orientation == 270:
            self.x -= steps
        else:
            # To Do: Implement forward driving for diagonal orientation
            print("Orientation not implemented")
        self.historyMap[self.x][self.y] += 1

    def turnLeft(self, steps):
        # steps: number of 45 Degree Rotations of the car
        # actually turn the car
        self.car.turnLeft(steps)
        # update orientation
        self.orientation -= steps * 45
        if self.orientation < 0:
            self.orientation += 360

    def turnRight(self, steps):
        # steps: number of 45 Degree Rotations of the car
        # actually turn the car
        self.car.turnRight(steps)
        # update orientation
        self.orientation += steps * 45
        if self.orientation >= 360:
            self.orientation -= 360

    def virtualTurnRight(self, steps):
        # update orientation
        self.orientation += steps * 45
        if self.orientation >= 360:
            self.orientation -= 360

    def virtualTurnLeft(self, steps):
        # update orientation
        self.orientation -= steps * 45
        if self.orientation < 0:
            self.orientation += 360

    def stop(self):
        # self.car.stop()
        return

    def updateMap(self):
        self.updateBluetoothMap()
        nr_new_datapoints = self.data_storage.get_nr_rows() - self.numberOfEvaluatedPoints
        for i in range(0, nr_new_datapoints):
            self.evaluate_new_data()

        for xCord in range(0, self.map_size - 1):
            for yCord in range(0, self.map_size - 1):

                if self.dataMap[xCord][yCord] >= self.thresholdBoarder:
                    # If enaugh datapoints at coordinate
                    self.Virtualmap[xCord][yCord] = 3  # declare Coordinate a border

                if ((abs(yCord - self.y) < self.bubble_size) and (abs(xCord - self.x) < self.bubble_size) and (
                        self.dataMap[xCord][yCord] < self.thresholdBoarder) and not self.Virtualmap[xCord][
                                                                                        yCord] == 3 and not
                self.Virtualmap[xCord][yCord] == 4) or self.historyMap[xCord][yCord] >= 1:
                    # if coordinate inside buble(rectangle) AND not to many datapoints at coordinate
                    self.Virtualmap[xCord][yCord] = 2  # declare as dicovered and free

                if (((abs(yCord - self.y) == self.bubble_size and abs(xCord - self.x) <= self.bubble_size) or (
                        abs(xCord - self.x) == self.bubble_size and abs(yCord - self.y) <= self.bubble_size)) and (
                        self.dataMap[xCord][yCord] < self.thresholdBoarder) and (self.Virtualmap[xCord][yCord] == 0)):
                    # if coordinate a edge of bubble AND undiscovered AND not to many datapoints at coordinate
                    self.Virtualmap[xCord][yCord] = 1  # declare as virtual border

    def updateMapFromObjectMap(self):
        squareSizeObjectMap = int(self.map_size / 20)  # Todo: replace 20 by some constant and make squareSizeObjectMap a global constant
        for xCord in range(0, self.map_size):
            for yCord in range(0, self.map_size):
                if self.objectMap[int(xCord / squareSizeObjectMap)][int(yCord / squareSizeObjectMap)] == 1:
                    self.Virtualmap[xCord][yCord] = 4

    def detectObject(self):
        squareSizeObjectMap = int(self.map_size / 20)  # Todo: replace 20 by some constant
        numberOfCoordinatesPerSquare = squareSizeObjectMap * squareSizeObjectMap
        for m in range(0, 20):
            for n in range(0, 20):
                borders = 0
                percentageOfborders = 0
                for xCord in range(m * squareSizeObjectMap, m * squareSizeObjectMap + squareSizeObjectMap):
                    for yCord in range(n * squareSizeObjectMap, n * squareSizeObjectMap + squareSizeObjectMap):
                        if self.Virtualmap[xCord][yCord] == 3:
                            borders += 1
                percentageOfborders = borders / numberOfCoordinatesPerSquare
                if percentageOfborders >= self.thresholdObject:
                    self.objectMap[m][n] = 1

    def getPath(self, startingX, startingY, targetX, targetY):
        squareSizeObjectMap = int(self.map_size / 20) # Todo: replace 20 by some constant same for all values below
        # calculating Start and Target in Object Map
        # Object Map is used for pathfinding in order to reduce calculating time and bugs
        startingSquareX = int(startingX / squareSizeObjectMap)
        startingSquareY = int(startingY / squareSizeObjectMap)
        targetSquareX = int(targetX / squareSizeObjectMap)
        targetSquareY = int(targetY / squareSizeObjectMap)

        graph = np.zeros((20, 20, 20, 20))  # (xCord,YCord,Xcord, ycord)
        distance = [np.inf] * 400

        distance[self.getNo(startingSquareX, startingSquareY)] = 0
        prev = [None] * 400
        que = [0] * 400

        # Creating the graph
        for xCord in range(0, 20):
            for yCord in range(0, 20):
                if self.objectMap[xCord][yCord] == 0:
                    if xCord >= 1:
                        if self.objectMap[xCord - 1][yCord] == 0:
                            graph[xCord][yCord][xCord - 1][yCord] = 1
                    if xCord <= 18:
                        if self.objectMap[xCord + 1][yCord] == 0:
                            graph[xCord][yCord][xCord + 1][yCord] = 1
                    if yCord >= 1:
                        if self.objectMap[xCord][yCord - 1] == 0:
                            graph[xCord][yCord][xCord][yCord - 1] = 1
                    if yCord <= 18:
                        if self.objectMap[xCord][yCord + 1] == 0:
                            graph[xCord][yCord][xCord][yCord + 1] = 1
                    que[self.getNo(xCord, yCord)] = 1
        while not que == [0] * 400:
            u = 0
            for i in range(0, 400):
                if que[i] == 1:
                    if distance[i] < distance[u]:
                        u = i
            que[u] = 0

            for m in range(0, 20):
                for n in range(0, 20):
                    if graph[self.getxCord(u)][self.getyCord(u)][m][n] == 1:
                        if que[self.getNo(m, n)] == 1:
                            alternativ = distance[u] + 1
                            if alternativ < distance[self.getNo(m, n)]:
                                distance[self.getNo(m, n)] = alternativ
                                prev[self.getNo(m, n)] = u
        path = [self.getNo(targetSquareX, targetSquareY)]
        search = self.getNo(targetSquareX, targetSquareY)

        while not prev[search] == None:
            search = prev[search]

            # self.objectMap[self.getxCord(search)][self.getyCord(search)] =1
            path.append(search)
        return path

    # SupportFunctions for get Path
    def getNo(self, xCord, yCord):
        return xCord * 20 + yCord

    def getxCord(self, num):
        xCord = int(num / 20)
        return xCord

    def getyCord(self, num):
        yCord = num % 20
        return yCord

    def getMiddleSquare(self, xCord, yCord):
        # Input in Object Map Coordinates, Output in normal Coordinates
        squareSizeObjectMap = int(self.map_size / 20)
        return int((xCord + 0.5) * squareSizeObjectMap), int((yCord + 0.5) * squareSizeObjectMap)

    def driveFromTo(self, startingX, startingY, targetX, targetY):
        # this is just for straight lines
        # dont go for to big differences between start and target, checks for borders only once
        # Returns if the driving was succesfull(True), or if there was a border(False)
        if startingX == targetX:
            if startingY <= targetY:
                self.turnToOrientation(self.orientation, 0)
                if not self.detectBorderInFront():
                    self.driveForward(int((targetY - startingY) / 2))
                    self.updateMap()
                else:
                    return False
                if not self.detectBorderInFront():
                    self.driveForward(int((targetY - startingY) / 2))
                    self.driveForward((targetY - startingY) % 2)
                else:
                    return False
            elif startingY > targetY:
                self.turnToOrientation(self.orientation, 180)
                if not self.detectBorderInFront():
                    self.driveForward(int((-targetY + startingY) / 2))
                    self.updateMap()
                else:
                    return False
                if not self.detectBorderInFront():
                    self.driveForward(int((-targetY + startingY) / 2))
                    self.driveForward((-targetY + startingY) % 2)
                else:
                    return False
        elif targetY == startingY:
            if startingX <= targetX:
                self.turnToOrientation(self.orientation, 90)
                # self.driveForward(targetX-startingX)
                if not self.detectBorderInFront():
                    self.driveForward(int((targetX - startingX) / 2))
                    self.updateMap()
                else:
                    return False
                if not self.detectBorderInFront():
                    self.driveForward(int((targetX - startingX) / 2))
                    self.driveForward((targetX - startingX) % 2)
                else:
                    return False

            elif startingX > targetX:
                self.turnToOrientation(self.orientation, 270)
                # self.driveForward(startingX-targetX)
                if not self.detectBorderInFront():
                    self.driveForward(int((startingX - targetX) / 2))
                    self.updateMap()
                else:
                    return False
                if not self.detectBorderInFront():
                    self.driveForward(int((startingX - targetX) / 2))
                    self.driveForward((startingX - targetX) % 2)
                else:
                    return False

        else:
            print("Error in driveFromTo: Target and Start are no in the same line")
        return True

    def turnToOrientation(self, startingOrientation, targetOrientation):
        if abs(startingOrientation - targetOrientation) == 180:
            self.turnLeft(4)
        elif startingOrientation - targetOrientation == 0:
            self.turnLeft(0)
        elif ((startingOrientation - targetOrientation) == 90 or (startingOrientation - targetOrientation) == -270):
            self.turnLeft(2)
        elif ((startingOrientation - targetOrientation) == -90 or (startingOrientation - targetOrientation) == 270):
            self.turnRight(2)
        else:
            print("Error in Turn to Orientation")

    def showVirtualMap(self):
        rotMap = np.zeros((self.map_size, self.map_size))
        for m in range(0, self.map_size):
            for n in range(0, self.map_size):
                rotMap[m][n] = self.Virtualmap[n][m]
        # plt.imshow(rotMap, origin='lower')
        # plt.colorbar()
        # plt.show()

    def showHistoryMap(self):
        rotMap = np.zeros((self.map_size, self.map_size))
        for m in range(0, self.map_size):
            for n in range(0, self.map_size):
                rotMap[m][n] = self.historyMap[n][m]
        # plt.imshow(rotMap, origin='lower')
        # plt.colorbar()
        # plt.show()

    def evaluate_new_data(self):
        dataArray = self.data_storage.to_array()
        # sensor 1 evaluation, sensor to the left
        if (dataArray[self.numberOfEvaluatedPoints][4] <= 4 * self.bubble_size * self.resolutuion):
            if self.orientation == 0:
                    self.dataMap[self.x - int(dataArray[self.numberOfEvaluatedPoints][4] / self.resolutuion)][self.y] += 1
            elif self.orientation == 90:
                    self.dataMap[self.x][self.y + int(dataArray[self.numberOfEvaluatedPoints][4] / self.resolutuion)] += 1
            elif self.orientation == 180:
                    self.dataMap[self.x + int(dataArray[self.numberOfEvaluatedPoints][4] / self.resolutuion)][self.y] += 1
            elif self.orientation == 270:
                    self.dataMap[self.x][self.y - int(dataArray[self.numberOfEvaluatedPoints][4] / self.resolutuion)] += 1

        # sensor 2 evaluation, sensor to the front

        # if dataArray[self.numberOfEvaluatedPoints][5] <= 2 * self.bubble_size * self.resolutuion:
        #     if self.orientation == 270:
        #         self.dataMap[self.x - int(dataArray[self.numberOfEvaluatedPoints][5] / self.resolutuion)][self.y] += 1
        #     elif self.orientation == 0:
        #         self.dataMap[self.x][self.y + int(dataArray[self.numberOfEvaluatedPoints][5] / self.resolutuion)] += 1
        #     elif self.orientation == 90:
        #         self.dataMap[self.x + int(dataArray[self.numberOfEvaluatedPoints][5] / self.resolutuion)][self.y] += 1
        #     elif self.orientation == 180:
        #             self.dataMap[self.x][self.y - int(dataArray[self.numberOfEvaluatedPoints][5] / self.resolutuion)] += 1

        # sensor3 evaluation, sensor to the right

        if (dataArray[self.numberOfEvaluatedPoints][6] <= 4 * self.bubble_size * self.resolutuion):
            if self.orientation == 180:
                self.dataMap[self.x - int(dataArray[self.numberOfEvaluatedPoints][6] / self.resolutuion)][self.y] += 1
            elif self.orientation == 270:
                self.dataMap[self.x][self.y + int(dataArray[self.numberOfEvaluatedPoints][6] / self.resolutuion)] += 1
            elif self.orientation == 0:
                self.dataMap[self.x + int(dataArray[self.numberOfEvaluatedPoints][6] / self.resolutuion)][self.y] += 1
            elif self.orientation == 90:
                self.dataMap[self.x][self.y - int(dataArray[self.numberOfEvaluatedPoints][6] / self.resolutuion)] += 1

        self.numberOfEvaluatedPoints += 1

    def returnPosition(self):
        return self.x, self.y, self.orientation

    def updateBluetoothMap(self):
        # Get RSSI Measurement
        self.DistanceBluetooth.run()
        # self.DistanceBluetooth.run() # used for extra de
        if self.BluetoothMap[self.x][self.y] == 0:
            # Add RSSI value to map
            self.BluetoothMap[self.x][self.y] = self.DistanceBluetooth.getAverageRSSI()
        else:
            # If there is already a value, take the average
            self.BluetoothMap[self.x][self.y] = 0.5*(self.BluetoothMap[self.x][self.y] + self.DistanceBluetooth.getAverageRSSI())

    def returnToBluetoothDevice(self):

        # Mark all 0's as -100, so max() returns highest RSSI and does not take the empty 0's into account
        for idx, x in np.ndenumerate(self.BluetoothMap):
            if x == 0:
                self.BluetoothMap[idx] = -100

        # find x and y index of max RSSI value
        index = np.argmax(self.BluetoothMap)
        indexX = int(index / self.map_size)
        indexY = index % self.map_size

        # Drive towards it
        # TODO: better pathfinding than just 2 straight lines
        self.driveFromTo(self.x, self.y, self.x, indexY)
        self.driveFromTo(self.x, self.y, indexX, self.y)

    def save_data_map(self, filename: str = "datamap.csv", output_dir=None):
        """Save the data to the disk in the specified format."""
        # data_array = self.to_array()
        if output_dir is None:  # If no other path is given, use the data folder
            output_dir = Path(__file__).parents[1] / DATA_FOLDER

        try:
            extension = filename.split('.')[-1]  # Get the last element after a dot (ususally extension)
        except IndexError:
            raise ValueError("No proper filename extension detected. Please add an extension (e.g. '.txt.' or '.csv').")

        filepath = output_dir / filename

        if extension == 'txt':
            np.savetxt(filepath, self.dataMap, fmt='%d')
        elif extension == 'csv':
            np.savetxt(filepath, self.dataMap, delimiter='  ', fmt='%d')
        elif extension == 'npy':
            np.save(filepath, self.dataMap)
        else:
            raise ValueError("Extension '.%s' not supported." % extension)

    def save_history_map(self, filename: str = "historymap.csv", output_dir=None):
        """Save the data to the disk in the specified format."""
        # data_array = self.to_array()
        if output_dir is None:  # If no other path is given, use the data folder
            output_dir = Path(__file__).parents[1] / DATA_FOLDER

        try:
            extension = filename.split('.')[-1]  # Get the last element after a dot (ususally extension)
        except IndexError:
            raise ValueError("No proper filename extension detected. Please add an extension (e.g. '.txt.' or '.csv').")

        filepath = output_dir / filename

        if extension == 'txt':
            np.savetxt(filepath, self.historyMap, fmt='%d')
        elif extension == 'csv':
            np.savetxt(filepath, self.historyMap, delimiter='   ', fmt='%d')
        elif extension == 'npy':
            np.save(filepath, self.historyMap)
        else:
            raise ValueError("Extension '.%s' not supported." % extension)

    def save_virtual_map(self, filename: str = "virtualmap.csv", output_dir=None):
        """Save the data to the disk in the specified format."""
        # data_array = self.to_array()
        if output_dir is None:  # If no other path is given, use the data folder
            output_dir = Path(__file__).parents[1] / DATA_FOLDER

        try:
            extension = filename.split('.')[-1]  # Get the last element after a dot (ususally extension)
        except IndexError:
            raise ValueError("No proper filename extension detected. Please add an extension (e.g. '.txt.' or '.csv').")

        filepath = output_dir / filename

        if extension == 'txt':
            np.savetxt(filepath, self.Virtualmap, fmt='%d')
        elif extension == 'csv':
            np.savetxt(filepath, self.Virtualmap, delimiter='   ', fmt='%d')
        elif extension == 'npy':
            np.save(filepath, self.Virtualmap)
        else:
            raise ValueError("Extension '.%s' not supported." % extension)

    def save_bluetooth_map(self, filename: str = "bluetoothmap.csv", output_dir=None):
        """Save the data to the disk in the specified format."""
        # data_array = self.to_array()
        if output_dir is None:  # If no other path is given, use the data folder
            output_dir = Path(__file__).parents[1] / DATA_FOLDER

        try:
            extension = filename.split('.')[-1]  # Get the last element after a dot (ususally extension)
        except IndexError:
            raise ValueError("No proper filename extension detected. Please add an extension (e.g. '.txt.' or '.csv').")

        filepath = output_dir / filename

        if extension == 'txt':
            np.savetxt(filepath, self.BluetoothMap, fmt='%d')
        elif extension == 'csv':
            np.savetxt(filepath, self.BluetoothMap, delimiter='   ', fmt='%d')
        elif extension == 'npy':
            np.save(filepath, self.BluetoothMap)
        else:
            raise ValueError("Extension '.%s' not supported." % extension)