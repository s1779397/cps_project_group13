"""
Original author: Roche Christopher
URL: https://github.com/rocheparadox/Kalman-Filter-Python-for-mpu6050

Edited by: Jelle Hierck
Date: January 2021
Summary of changes: Added a threaded AngleMeasurer class which keeps track of the angle. Refactored many names and
    cleaned the code. Adjusted much code to personal style.

MIT License

Copyright (c) 2020 Roche Christopher

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
from imu_mpu6050 import MPU6050

import time
from math import atan, atan2, sqrt
from math import degrees as rad2deg
import threading


class KalmanCalculator(threading.Thread):
    MEASUREMENT_INTERVAL = 0.05  # Accelerometer measurement interval

    def __init__(self, name, mpu: MPU6050):
        # threading.Thread.__init__(self, name=name, daemon=)
        super().__init__(name=name, daemon=True)
        self.name = name
        self.mpu = mpu

        self._kalman_x = KalmanAngle()
        self._kalman_y = KalmanAngle()

        self._roll = 0  # Variable which holds the processed roll (x angle)
        self._pitch = 0  # variable which holds the processed pitch (y angle)
        self._kal_angle_x = 0
        self._kal_angle_y = 0
        self._compl_angle_x = 0
        self._compl_angle_y = 0
        self._gyro_angle_x = 0
        self._gyro_angle_y = 0

        self._timer = 0

        self._read_lock = threading.Lock()  # lock for thread-safe access to roll and pitch
        self.interrupted = False  # Flag to interrupt the thread

    def get_roll(self) -> float:
        """Get the roll (x angle)"""
        with self._read_lock:
            return self._roll

    def get_pitch(self) -> float:
        """Get the pitch (y angle)"""
        with self._read_lock:
            return self._pitch

    def get_angle_x(self) -> float:
        """Get the x angle (roll)"""
        return self.get_roll()

    def get_angle_y(self) -> float:
        """Get the y angle (pitch)"""
        return self.get_pitch()

    def get_angles(self) -> tuple:
        """Get a tuple of angles"""
        return self.get_roll(), self.get_pitch()

    def measure_angle(self) -> tuple:
        """Measure at what angle the accelerometer currently is using the information of previous measurements."""
        # Read Accelerometer raw value
        acc_data = self.mpu.get_accel_data()
        acc_x = acc_data['x']
        acc_y = acc_data['y']
        acc_z = acc_data['z']

        # Read Gyroscope raw value
        gyro_data = self.mpu.get_gyro_data()
        gyro_x = gyro_data['x']
        gyro_y = gyro_data['y']
        gyro_z = gyro_data['z']  # Not used

        dt = time.time() - self._timer
        self._timer = time.time()

        roll = rad2deg(atan2(acc_y, acc_z))
        pitch = rad2deg(atan(-acc_x / sqrt((acc_y ** 2) + (acc_z ** 2))))

        gyro_rate_x = gyro_x / self.mpu.get_gyro_scale_modifier()  # Scale the gyroscope
        gyro_rate_y = gyro_y / self.mpu.get_gyro_scale_modifier()  # Scale the gyroscope

        if (roll < -90 and self._kal_angle_x > 90) or (roll > 90 and self._kal_angle_x < -90):
            self._kalman_x.setAngle(roll)
            self._compl_angle_x = roll
            self._kal_angle_x = roll
            self._gyro_angle_x = roll
        else:
            self._kal_angle_x = self._kalman_x.getAngle(roll, gyro_rate_x, dt)

        if abs(self._kal_angle_y) > 90:  # If the pitch is bigger than 90 degrees, invert it
            gyro_rate_y = -gyro_rate_y
            self._kal_angle_y = self._kalman_y.getAngle(pitch, gyro_rate_y, dt)

        # angle = (rate of change of angle) * change in time
        self._gyro_angle_x = gyro_rate_x * dt
        self._gyro_angle_y = gyro_rate_y * dt

        # compAngle = constant * (old_compAngle + angle_obtained_from_gyro) + constant * angle_obtained from accelerometer
        self._compl_angle_x = 0.93 * (self._compl_angle_x + gyro_rate_x * dt) + 0.07 * roll
        self._compl_angle_y = 0.93 * (self._compl_angle_y + gyro_rate_y * dt) + 0.07 * pitch

        # Adjust angles to range (-180, 180)
        if (self._gyro_angle_x < -180) or (self._gyro_angle_x > 180):
            self._gyro_angle_x = self._kal_angle_x
        if (self._gyro_angle_y < -180) or (self._gyro_angle_y > 180):
            self._gyro_angle_y = self._kal_angle_y

        with self._read_lock:
            self._pitch = self._compl_angle_y
            self._roll = self._compl_angle_x
            return self._roll, self._pitch  # Return the x and y angles

    def run(self):
        """Overridden method of the Thread class. Run the thread using .start()"""
        self.measure_until_interrupted()

    def measure_until_interrupted(self, interval=None, log_output=False):
        """
        Continuously measure the angle recorded by the accelerometer. This will only be stopped once self.interrupt is
        set to True.

        :param interval: Time in between measurements. Defaults to MEASUREMENT_INTERVAL
        :param log_output: Whether the output should be logged (it is recommended to set a higher interval when using
        this feature). Defaults to False.
        """
        if interval is None:
            interval = self.MEASUREMENT_INTERVAL

        acc_data = self.mpu.get_accel_data()
        acc_x = acc_data['x']
        acc_y = acc_data['y']
        acc_z = acc_data['z']

        angle_x = rad2deg(atan2(acc_y, acc_z))
        angle_y = rad2deg(atan(-acc_x / sqrt((acc_y ** 2) + (acc_z ** 2))))

        self._kalman_x.setAngle(angle_x)
        self._kalman_y.setAngle(angle_y)
        self._gyro_angle_x = angle_x
        self._gyro_angle_y = angle_y
        self._compl_angle_x = angle_x
        self._compl_angle_y = angle_y

        self._timer = time.time()

        while self.interrupted is False:  # Measure forever until the self.interrupted flag is set to True
            roll, pitch = self.measure_angle()
            if log_output is True:
                print('roll (x): %0.1f, pitch (y): %0.1f' % (roll, pitch))
            time.sleep(interval)

    # def measure_angle_continuously(self):
    #     kalmanX = KalmanAngle()
    #     kalmanY = KalmanAngle()
    #     kalAngleX = 0
    #     kalAngleY = 0
    #
    #     # time.sleep(1)
    #
    #     # Read Accelerometer raw value
    #     acc_data = self.mpu.get_accel_data()
    #     acc_x = acc_data['x']
    #     acc_y = acc_data['y']
    #     acc_z = acc_data['z']
    #
    #     roll = rad2deg(atan2(acc_y, acc_z))
    #     pitch = rad2deg(atan(-acc_x / sqrt((acc_y ** 2) + (acc_z ** 2))))
    #     kalmanX.setAngle(roll)
    #     kalmanY.setAngle(pitch)
    #
    #     gyroXAngle = roll
    #     gyroYAngle = pitch
    #     compAngleX = roll
    #     compAngleY = pitch
    #
    #     timer = time.time()
    #     flag = 0
    #
    #     while True:
    #
    #         if flag > 100:  # Problem with the connection
    #             print("There is a problem with the connection")
    #             flag = 0
    #             continue
    #         try:
    #             # Read Accelerometer raw value
    #             acc_data = self.mpu.get_accel_data()
    #             acc_x = acc_data['x']
    #             acc_y = acc_data['y']
    #             acc_z = acc_data['z']
    #
    #             # Read Gyroscope raw value
    #             gyro_data = self.mpu.get_gyro_data()
    #             gyro_x = gyro_data['x']
    #             gyro_y = gyro_data['y']
    #             gyro_z = gyro_data['z']
    #
    #             dt = time.time() - timer
    #             timer = time.time()
    #
    #             roll = rad2deg(atan2(acc_y, acc_z))
    #             pitch = rad2deg(atan(-acc_x / sqrt((acc_y ** 2) + (acc_z ** 2))))
    #
    #             gyroXRate = gyro_x / self.mpu.get_gyro_scale_modifier()  # Scale the gyroscope
    #             gyroYRate = gyro_y / self.mpu.get_gyro_scale_modifier()  # Scale the gyroscope
    #
    #             if (roll < -90 and kalAngleX > 90) or (roll > 90 and kalAngleX < -90):
    #                 kalmanX.setAngle(roll)
    #                 complAngleX = roll
    #                 kalAngleX = roll
    #                 gyroXAngle = roll
    #             else:
    #                 kalAngleX = kalmanX.getAngle(roll, gyroXRate, dt)
    #
    #             if abs(kalAngleY) > 90:  # If the pitch is bigger than 90 degrees
    #                 gyroYRate = -gyroYRate
    #                 kalAngleY = kalmanY.getAngle(pitch, gyroYRate, dt)
    #
    #             # angle = (rate of change of angle) * change in time
    #             gyroXAngle = gyroXRate * dt
    #             gyroYAngle = gyroYAngle * dt
    #
    #             # compAngle = constant * (old_compAngle + angle_obtained_from_gyro) + constant * angle_obtained from accelerometer
    #             compAngleX = 0.93 * (compAngleX + gyroXRate * dt) + 0.07 * roll
    #             compAngleY = 0.93 * (compAngleY + gyroYRate * dt) + 0.07 * pitch
    #
    #             if (gyroXAngle < -180) or (gyroXAngle > 180):
    #                 gyroXAngle = kalAngleX
    #             if (gyroYAngle < -180) or (gyroYAngle > 180):
    #                 gyroYAngle = kalAngleY
    #
    #             self.pitch = compAngleY
    #             self.roll = compAngleX
    #
    #             self.kal_angle_y = kalAngleY
    #             self.kal_angle_x = kalAngleX
    #             self.compl_angle_y = compAngleY
    #             self.compl_angle_x = compAngleX
    #
    #             print("Angle X: " + str(kalAngleX) + "   " + "Angle Y: " + str(kalAngleY))
    #
    #             time.sleep(0.005)
    #
    #         except Exception as exc:
    #             if (flag == 100):
    #                 print(exc)
    #             flag += 1


class KalmanAngle:

    def __init__(self):
        self.QAngle = 0.001
        self.QBias = 0.003
        self.RMeasure = 0.03
        self.angle = 0.0
        self.bias = 0.0
        self.rate = 0.0
        self.P = [[0.0, 0.0], [0.0, 0.0]]

    def getAngle(self, newAngle, newRate, dt):
        # step 1:
        self.rate = newRate - self.bias  # new_rate is the latest Gyro measurement
        self.angle += dt * self.rate

        # Step 2:
        self.P[0][0] += dt * (dt * self.P[1][1] - self.P[0][1] - self.P[1][0] + self.QAngle)
        self.P[0][1] -= dt * self.P[1][1]
        self.P[1][0] -= dt * self.P[1][1]
        self.P[1][1] += self.QBias * dt

        # Step 3: Innovation
        y = newAngle - self.angle

        # Step 4: Innovation covariance
        s = self.P[0][0] + self.RMeasure

        # Step 5:    Kalman Gain
        K = [0.0, 0.0]
        K[0] = self.P[0][0] / s
        K[1] = self.P[1][0] / s

        # Step 6: Update the Angle
        self.angle += K[0] * y
        self.bias += K[1] * y

        # Step 7: Calculate estimation error covariance - Update the error covariance
        P00Temp = self.P[0][0]
        P01Temp = self.P[0][1]

        self.P[0][0] -= K[0] * P00Temp
        self.P[0][1] -= K[0] * P01Temp
        self.P[1][0] -= K[1] * P00Temp
        self.P[1][1] -= K[1] * P01Temp

        return self.angle

    def setAngle(self, angle):
        self.angle = angle

    def setQAngle(self, QAngle):
        self.QAngle = QAngle

    def setQBias(self, QBias):
        self.QBias = QBias

    def setRMeasure(self, RMeasure):
        self.RMeasure = RMeasure

    def getRate(self):
        return self.rate

    def getQAngle(self):
        return self.QAngle

    def getQBias(self):
        return self.QBias

    def getRMeasure(self):
        return self.RMeasure
