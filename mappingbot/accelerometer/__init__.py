import threading

from imu_mpu6050 import MPU6050
import time


# from mappingbot.accelerometer.kalman import KalmanCalculator


class Accelerometer:
    SLEEP_TIME = 0.2

    def __init__(self, name, auto_reset_time=0):
        self.name = name
        self.mpu = MPU6050()
        # self.angle_tracker = KalmanCalculator(name=name, mpu=self.mpu)
        self.angle_tracker = SimpleAngleCalculator(name=name, mpu=self.mpu, auto_reset=auto_reset_time)

    def get_acc(self) -> dict:
        """Return raw acceleration data."""
        return self.mpu.get_accel_data()

    def get_gyro(self) -> dict:
        """Return raw gyroscope data."""
        return self.mpu.get_gyro_data()

    def start(self):
        """Start the sensor thread"""
        self.angle_tracker.start()
        time.sleep(0.1)

    def stop(self):
        """Stop the sensor thread"""
        self.angle_tracker.interrupt()

    def wait_until_angle(self, target_angle=90):
        """Wait until the sensor has turned to a specific angle."""
        # if isinstance(self.angle_tracker, KalmanCalculator):
        #     initial_angle_x, initial_angle_y = self.angle_tracker.get_angles()
        #     print("initial angle x: %0.1f, initial angle y: %0.1f" % (initial_angle_x, initial_angle_y))
        #     angle_x = initial_angle_x
        #     angle_y = initial_angle_y
        #     while angle_x - initial_angle_x < target_angle and angle_y - initial_angle_y < target_angle:
        #         angle_x, angle_y = self.angle_tracker.get_angles()
        #         print("angle x: %0.1f, angle y: %0.1f" % (angle_x, angle_y))
        #         time.sleep(self.SLEEP_TIME)
        #     print("DONE")
        #     print("final angle x: %0.1f, final angle y: %0.1f" % (angle_x, angle_y))
        # elif isinstance(self.angle_tracker, SimpleAngleCalculator):
        initial_angle_x, initial_angle_y, initial_angle_z = self.angle_tracker.get_angles()
        print("initial angle x: %0.1f, initial angle y: %0.1f, initial angle z: %0.1f" % (
            initial_angle_x, initial_angle_y, initial_angle_z))
        angle_x = initial_angle_x
        angle_y = initial_angle_y
        angle_z = initial_angle_z
        while angle_x - initial_angle_x < target_angle and angle_y - initial_angle_y < target_angle and angle_z - initial_angle_z < target_angle:
            angle_x, angle_y, angle_z = self.angle_tracker.get_angles()
            print("angle x: %0.1f, angle y: %0.1f , angle z: %0.1f" % (angle_x, angle_y, angle_z))
            time.sleep(self.SLEEP_TIME)
        print("DONE")
        print("final angle x: %0.1f, final angle y: %0.1f, final angle z: %0.1f" % (angle_x, angle_y, angle_z))

    def get_angles(self):
        return self.angle_tracker.get_angles()

    def reset_angles(self):
        self.angle_tracker.reset()


class SimpleAngleCalculator(threading.Thread):
    SAMPLE_RATE = 100  # Hz

    def __init__(self, name, mpu, sample_rate=SAMPLE_RATE, auto_reset=0):
        """
        Initialze this simple angle calculator using only the gyroscope and no additional data processing (no Kalman).

        :param name: The object and thread name.
        :param mpu: The MPU6050 object.
        :param sample_rate: Sample rate of the gyroscope in Hz. Default: 100.
        :param auto_reset: Time before auto reset in seconds. When 0 or negative, the reset will not take place
        automatically. Default: 0
        """
        super().__init__(name=name, daemon=True)
        self.interrupted = False  # Interrupt flag for stopping the thread
        self.read_write_lock = threading.RLock()  # Lock for reading and writing angles

        self.name = name  # Name of this object
        self.mpu = mpu  # MPU object (MPU6050)
        self.sample_time = 1 / sample_rate

        if auto_reset > 0:
            self.auto_reset = auto_reset
        else:
            self.auto_reset = 0

        self.time_since_reset = time.time()
        self.time_prev = time.time()
        self._angle_x = 0
        self._angle_y = 0
        self._angle_z = 0

    def run(self):
        """Overridden method of Thread. Use .start() to start the thread."""
        self.measure()

    def add_measurement(self):
        """Add a single measurement to the current angles, based on the integrated gyroscope measured angular rate."""
        with self.read_write_lock:
            gyro = self.mpu.get_gyro_data()
            dt = time.time() - self.time_prev  # Calculate exact time passed
            self.time_prev = time.time()
            self._angle_x += gyro['x'] * dt
            self._angle_x += gyro['y'] * dt
            self._angle_x += gyro['z'] * dt

    def measure(self):
        """Measure continuously until self.interrupted is set to True"""
        self.reset()
        time.sleep(self.sample_time)
        while self.interrupted is False:
            self.add_measurement()
            time.sleep(self.sample_time)

            # Check whether the angles should reset automatically
            if self.auto_reset != 0 and time.time() - self.time_since_reset > self.auto_reset:
                self.reset()

    def get_angles(self):
        """Read the angles currently stored."""
        with self.read_write_lock:
            return self._angle_x, self._angle_y, self._angle_z

    def reset(self):
        """Reset the angle values and measure."""
        with self.read_write_lock:
            self.time_since_reset = time.time()
            self.time_prev = time.time()
            self._angle_x = 0
            self._angle_x = 0
            self._angle_x = 0

    def interrupt(self):
        """Stop the thread and wait until is is joined"""
        self.interrupted = True
        self.join()


if __name__ == '__main__':
    accelerometer = Accelerometer("accelerometer", kalman=False, auto_reset_time=0)
    accelerometer.start()
    # accelerometer.wait_until_angle(90)

    # sleep_time = 0.01

    # timer = time.time()
    # i = 0
    # j = 0

    # gyro_x = accelerometer.get_gyro()['x'] / accelerometer.mpu.get_gyro_scale_modifier()
    # dt = time.time() - timer
    # angle = gyro_x * dt
    # print("angle: %0.1f" % angle)
    # time.sleep(sleep_time)

    try:
        while True:
            # angle_x, angle_y = accelerometer.angle_tracker.get_angles()
            # print("angle x: %0.1f, angle y: %0.1f" % (angle_x, angle_y))
            # time.sleep(0.3)

            angle_x, angle_y, angle_z = accelerometer.angle_tracker.get_angles()
            print("angle x: %0.1f, angle y: %0.1f, angle z: %0.1f" % (angle_x, angle_y, angle_z))
            time.sleep(0.3)

            # gyro_x = accelerometer.get_gyro()['x']  # / accelerometer.mpu.get_gyro_scale_modifier()
            # dt = time.time() - timer
            # timer = time.time()
            # angle += gyro_x * dt
            # i += 1
            # j += 1
            # if i >= 30:
            #     print("angle: %0.1f" % angle)
            #     i = 0
            # if j >= 500:
            #     print('reset')
            #     j = 0
            #     angle = 0
            # time.sleep(sleep_time)

    except KeyboardInterrupt:
        print('Stopping sensor')
        accelerometer.stop()
    finally:
        print('Done')
