from numpy import pi as PI
from pathlib import Path

# ----- GPIO Pin Configurations (assuming BCM) -----

MOTOR_LEFT_FORWARD = 17  # was 20
MOTOR_LEFT_BACKWARD = 27  # was 26
ENCODER_LEFT = 22  # was 23

MOTOR_RIGHT_FORWARD = 18  # was 12
MOTOR_RIGHT_BACKWARD = 23  # was 13
ENCODER_RIGHT = 24

ULTRASOUND_ECHO_LEFT = 16
ULTRASOUND_TRIGGER_LEFT = 13  # was 17

ULTRASOUND_ECHO_FORWARD = 20
ULTRASOUND_TRIGGER_FORWARD = 19  # was 27

ULTRASOUND_ECHO_RIGHT = 21
ULTRASOUND_TRIGGER_RIGHT = 26  # was 22

# ----- Numerical Constants -----

WHEEL_DIAMETER = 6.5  # Wheel diameter in cm
WHEEL_CIRCUMFERENCE = WHEEL_DIAMETER * PI  # wheel circumference in cm

BASE_WIDTH = 14.3  # Distance between both wheel centers in cm
WHEEL_BASE_CIRCUMFERENCE = BASE_WIDTH / 2 * PI  # Circumference of circle created when turning both wheels opposite

ENCODER_PPR = 40  # Encoder pulses per rotation (assuming both rising and falling edges)

SONIC_SPEED = 343 * 100  # Speed of sound in cm/s

# Folders and files
DATA_FOLDER = 'data'  # Default output folder for data
DATA_FOLDER_PATH = Path(__file__).parents[1] / DATA_FOLDER  # Absolute path to the data folder
FILENAME_BLUETOOTHMAP = 'bluetoothmap.csv'  # Default output filenames
FILENAME_DATAMAP = 'datamap.csv'
FILENAME_HISTORYMAP = 'historymap.csv'
FILENAME_MAPPINGDATA = 'mappingdata.csv'
FILENAME_VIRTUALMAP = 'virtualmap.csv'
