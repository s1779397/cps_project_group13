import RPi.GPIO as GPIO
import time

from constants import ENCODER_PPR, WHEEL_CIRCUMFERENCE
# from mappingbot.constants import ENCODER_PPR, WHEEL_CIRCUMFERENCE

GPIO.setmode(GPIO.BCM)


class Encoder:

    def __init__(self, name, encoder_pin: int):
        """Initialize some stuff"""
        self.name = name
        self.encoder_pin = encoder_pin
        self.pulse_count = 0
        self.start_time = time.time()
        self.time_passed = 0
        self.old_time_passed = 0
        self.old_pulse_count = 0

        GPIO.setup(self.encoder_pin, GPIO.IN, GPIO.PUD_DOWN)

    def callback_encoder(self, channel):
        """Interrupt handler, increases pulse_count and updates time"""
        self.time_passed = time.time() - self.start_time
        self.pulse_count += 1

    def get_pulse_count(self) -> int:
        """return pulse_count"""
        return self.pulse_count

    def reset_pulse_count(self):
        """reset pulse_count"""
        self.pulse_count = 0

    def reset_time(self):
        """reset time_passed and start_time"""
        self.time_passed = 0
        self.start_time = time.time()

    def get_current_pulse_speed(self) -> float:
        """Get average speed since last call in pulses/s"""
        if self.time_passed - self.old_time_passed != 0:  # If the time has changed
            speed = (self.pulse_count - self.old_pulse_count) / (self.time_passed - self.old_time_passed)
        else:
            speed = 0
        self.old_pulse_count = self.pulse_count
        self.old_time_passed = self.time_passed
        return speed

    def get_speed(self) -> float:
        """Convert pulse speed to cm/s"""
        pulse_per_second = self.get_current_pulse_speed()
        return pulse_per_second * WHEEL_CIRCUMFERENCE / ENCODER_PPR

    def init_callback(self):
        """Initialize the interrupt"""
        GPIO.add_event_detect(self.encoder_pin, GPIO.BOTH, callback=self.callback_encoder, bouncetime=10)


if __name__ == '__main__':
    try:
        encoder1 = Encoder(name='encoder1', encoder_pin=23)
        encoder2 = Encoder(name='encoder2', encoder_pin=24)
        encoder1.init_callback()
        encoder2.init_callback()
        while True:
            print("%s pulses: %d" % (encoder1.name, encoder1.get_pulse_count()))
            print("%s speed : %0.2f" % (encoder1.name, encoder1.get_speed()))
            print("%s pulses: %d" % (encoder2.name, encoder2.get_pulse_count()))
            print("%s speed : %0.2f" % (encoder2.name, encoder2.get_speed()))
            time.sleep(1)
    except KeyboardInterrupt:  # Exit by pressing CTRL + C
        print("Measurement stopped by User")
    finally:
        GPIO.cleanup()
