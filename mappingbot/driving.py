# Libraries
import RPi.GPIO as GPIO
import time
import math

from encoder import Encoder
from constants import MOTOR_LEFT_FORWARD, MOTOR_RIGHT_FORWARD, MOTOR_RIGHT_BACKWARD, MOTOR_LEFT_BACKWARD, \
    ENCODER_LEFT, ENCODER_RIGHT, WHEEL_CIRCUMFERENCE, ENCODER_PPR, WHEEL_BASE_CIRCUMFERENCE, BASE_WIDTH

# from mappingbot.accelerometer import Accelerometer
# from mappingbot.encoder import Encoder
# from mappingbot.constants import MOTOR_LEFT_FORWARD, MOTOR_RIGHT_FORWARD, MOTOR_RIGHT_BACKWARD, MOTOR_LEFT_BACKWARD, \
#     ENCODER_LEFT, ENCODER_RIGHT, WHEEL_CIRCUMFERENCE, ENCODER_PPR, WHEEL_BASE_CIRCUMFERENCE, BASE_WIDTH

GPIO.setmode(GPIO.BCM)

class Driving:

    def __init__(self):
        self.setpointPulseCountLeft = 0
        self.setpointPulseCountRight = 0
        self.currentPulseCountLeft = 0
        self.currentPulseCountRight = 0
        self.directionLeftWheel = True  # true = forward
        self.directionRightWheel = True  # true = forward
        self.pulsesPerRotation = ENCODER_PPR
        self.pulsesPerRadian = 2 * ENCODER_PPR / (2 * math.pi)
        self.speed = 0  # speed in PWM dutycycle, ranges from 0 to 100
        self.rotationspeed = 45  # speed in PWM dutycycle
        self.wheelDistance = BASE_WIDTH
        self.x = 0
        self.y = 0
        self.orientation = 0
        self.setpointX = 0
        self.setpointY = 0
        self.setpointOrientation = 0
        self.oldPCL = 0
        self.oldPCR = 0
        self.oldPCLDrive = 0
        self.oldPCRDrive = 0
        self.leftPWM = 0
        self.rightPWM = 0
        self.increaseLeftPWM = 0
        self.increaseRightPWM = 0
        self.i = 0
        self.move = 0
        self.angleSum = 0

        GPIO.setup(MOTOR_LEFT_FORWARD, GPIO.OUT)
        GPIO.setup(MOTOR_LEFT_BACKWARD, GPIO.OUT)
        GPIO.setup(MOTOR_RIGHT_FORWARD, GPIO.OUT)
        GPIO.setup(MOTOR_RIGHT_BACKWARD, GPIO.OUT)

        self.leftPinF = GPIO.PWM(MOTOR_LEFT_FORWARD, 50)
        self.leftPinB = GPIO.PWM(MOTOR_LEFT_BACKWARD, 50)
        self.rightPinF = GPIO.PWM(MOTOR_RIGHT_FORWARD, 50)
        self.rightPinB = GPIO.PWM(MOTOR_RIGHT_BACKWARD, 50)
        self.leftPinF.start(0)
        self.leftPinB.start(0)
        self.rightPinF.start(0)
        self.rightPinB.start(0)
        self.encoderLeft = Encoder(name="encoderLeft", encoder_pin=ENCODER_LEFT)  # Will also setup the GPIO pin
        self.encoderRight = Encoder(name="encoderRight", encoder_pin=ENCODER_RIGHT)  # Will also setup the GPIO pin
        self.encoderLeft.init_callback()
        self.encoderRight.init_callback()

    def driveForward(self, distance, speed):
        print("forward")
        # update setpoint
        self.setpointX += math.cos(self.setpointOrientation) * distance
        self.setpointY += math.sin(self.setpointOrientation) * distance
        # reset pulse count
        self.encoderLeft.reset_pulse_count()
        self.encoderRight.reset_pulse_count()
        # set speed
        self.speed = speed
        # set wheel direction
        self.directionLeftWheel = True
        self.directionRightWheel = True
        # calculate setpoint
        self.setpointPulseCountLeft = int(distance / WHEEL_CIRCUMFERENCE * self.pulsesPerRotation)
        self.setpointPulseCountRight = int(distance / WHEEL_CIRCUMFERENCE * self.pulsesPerRotation)
        self.drive()

    def turnLeft(self, numberOf45degreeRotations):
        print("turnLeft")
        # update setpoint
        self.setpointOrientation -= numberOf45degreeRotations * math.pi / 4
        # reset pulse count
        self.encoderLeft.reset_pulse_count()
        self.encoderRight.reset_pulse_count()
        # set speed
        self.speed = self.rotationspeed
        # set direction wheels
        self.directionLeftWheel = False
        self.directionRightWheel = True
        # calculate setpoints
        self.setpointPulseCountLeft = 0.5 * self.pulsesPerRotation * abs(
            self.setpointOrientation - self.orientation) * self.wheelDistance / WHEEL_CIRCUMFERENCE
        self.setpointPulseCountRight = 0.5 * self.pulsesPerRotation * abs(
            self.setpointOrientation - self.orientation) * self.wheelDistance / WHEEL_CIRCUMFERENCE
        self.drive()

    def turnRight(self, numberOf45degreeRotations):
        print("turnRight")
        # update setpoint
        self.setpointOrientation += numberOf45degreeRotations * math.pi / 4
        # reset pulse count
        self.encoderLeft.reset_pulse_count()
        self.encoderRight.reset_pulse_count()
        # set speed
        self.speed = self.rotationspeed
        # set wheel direction
        self.directionLeftWheel = True
        self.directionRightWheel = False
        # calculate setpoints
        self.setpointPulseCountLeft = 0.5 * self.pulsesPerRotation * abs(
            self.setpointOrientation - self.orientation) * self.wheelDistance / WHEEL_CIRCUMFERENCE
        self.setpointPulseCountRight = 0.5 * self.pulsesPerRotation * abs(
            self.setpointOrientation - self.orientation) * self.wheelDistance / WHEEL_CIRCUMFERENCE
        self.drive()

    def getPWM(self):
        self.currentPulseCountLeft = self.encoderLeft.get_pulse_count()
        self.currentPulseCountRight = self.encoderRight.get_pulse_count()

        # Set orientation compensation gain depending on if the car is driving or turning
        # This works, but did not improve the wall following, so it is currently disabled
        orientationError = self.setpointOrientation - self.orientation
        if self.directionLeftWheel and self.directionRightWheel and abs(orientationError) > 0.05:
            gainOrientation = 25
        elif not self.directionLeftWheel and not self.directionRightWheel and abs(orientationError) > 0.1:
            gainOrientation = -20
        else:
            gainOrientation = 0
        gainOrientation = 0  # Remove to enable orientation compensation
        # Set PWM depending on pulsecount
        if self.currentPulseCountLeft >= self.setpointPulseCountLeft:
            self.leftPWM = 0
        elif self.currentPulseCountLeft >= self.setpointPulseCountLeft - 6:
            self.leftPWM = self.speed * 0.8
        elif self.currentPulseCountLeft >= self.setpointPulseCountLeft - 3:
            self.leftPWM = self.speed * 0.6
        else:
            self.leftPWM = self.speed
        # Set PWM depending on pulsecount
        if self.currentPulseCountRight >= self.setpointPulseCountRight:
            self.rightPWM = 0
        elif self.currentPulseCountRight >= self.setpointPulseCountRight - 6:
            self.rightPWM = self.speed * 0.8
        elif self.currentPulseCountRight >= self.setpointPulseCountRight - 3:
            self.rightPWM = self.speed * 0.6
        else:
            self.rightPWM = self.speed

        # When turning, stop when angle error becomes small
        if self.directionLeftWheel != self.directionRightWheel:
            # When rotating
            if self.directionRightWheel:
                orientationError *= -1
            if orientationError < 0.1:
                # Stop
                self.leftPWM = 0
                self.rightPWM = 0
            elif orientationError < 0.7:
                # Slow down
                self.leftPWM = self.speed * 0.5
                self.rightPWM = self.speed * 0.5
            else:
                self.leftPWM = self.speed
                self.rightPWM = self.speed

        # Increase PWM values from increase methods
        # and add orientation compensation
        if self.leftPWM > 20:
            self.leftPWM += gainOrientation * orientationError
            self.leftPWM += self.increaseLeftPWM
        if self.rightPWM > 20:
            self.rightPWM -= gainOrientation * orientationError
            self.rightPWM += self.increaseRightPWM

        # When PWM signal becomes too low, increase both sides. This prevents the car from getting stuck
        if 20 > self.leftPWM >= 12 or 20 > self.rightPWM >= 12:
            self.rightPWM += 10
            self.leftPWM += 10

        # set PWM boundaries
        if self.leftPWM > 100:
            self.leftPWM = 100
        elif self.leftPWM < 12:
            self.leftPWM = 0
        if self.rightPWM > 100:
            self.rightPWM = 100
        elif self.rightPWM < 12:
            self.rightPWM = 0

        return self.leftPWM, self.rightPWM, self.directionLeftWheel, self.directionRightWheel

    def drive(self):
        # Update PWM to motor driver according to getPWM() and calculate change in pulse counts
        leftDC, rightDC, dirLeft, dirRight = self.getPWM()
        diffPCL = self.currentPulseCountLeft - self.oldPCLDrive
        diffPCR = self.currentPulseCountRight - self.oldPCRDrive

        # set dutycycle for both motors
        if dirLeft:
            self.leftPinF.ChangeDutyCycle(leftDC)
            self.leftPinB.ChangeDutyCycle(0)
        else:
            self.leftPinB.ChangeDutyCycle(leftDC)
            self.leftPinF.ChangeDutyCycle(0)

        if dirRight:
            self.rightPinF.ChangeDutyCycle(rightDC)
            self.rightPinB.ChangeDutyCycle(0)
        else:
            self.rightPinB.ChangeDutyCycle(rightDC)
            self.rightPinF.ChangeDutyCycle(0)

        if dirLeft and dirRight:
            # moving forward
            diff = diffPCL - diffPCR
        elif dirLeft and not dirRight:
            # turning right
            diff = diffPCL + diffPCR
        elif not dirLeft and dirRight:
            # turning left
            diff = -diffPCL - diffPCR
        elif not dirLeft and not dirRight:
            # reverse
            diff = diffPCR - diffPCL

        # Update orientation
        self.orientation += diff * (WHEEL_CIRCUMFERENCE / self.pulsesPerRotation) / (
                self.wheelDistance * 2 * math.pi) * 2 * math.pi

        # Update position every 10 iterations
        if self.i >= 10:
            self.calcPos()
            self.i = 0
            self.angleSum = 0
        else:
            self.i += 1
            self.angleSum += self.orientation

        self.oldPCLDrive = self.currentPulseCountLeft
        self.oldPCRDrive = self.currentPulseCountRight

        # if not stopped, continue
        if not (leftDC == 0 and rightDC == 0):
            time.sleep(0.01)
            self.drive()
        else:
            if self.i != 0:
                self.calcPos()
            self.oldPCR = 0
            self.oldPCL = 0
            self.oldPCRDrive = 0
            self.oldPCLDrive = 0
            self.currentPulseCountLeft = 0
            self.currentPulseCountRight = 0
            self.increaseLeftPWM = 0
            self.increaseRightPWM = 0

            time.sleep(0.3)  # IMPORTANT, without, it becomes shittier

    def calcPos(self):
        # Updating the position faster gave unexpected results. This was probably cause because min/max functions were
        # used. Now the average between the two wheels is taken, this should no longer be a problem.
        # Note: in the results in the paper, max(diffPCL,diffPCR) was still used instead of (diffPCL+diffPCR)/2
        diffPCL = self.currentPulseCountLeft - self.oldPCL
        diffPCR = self.currentPulseCountRight - self.oldPCR
        dirLeft = self.directionLeftWheel
        dirRight = self.directionRightWheel
        # Get difference between wheels. And determine how far it moved
        if dirLeft and dirRight:
            # moving forward
            self.move = (diffPCL+diffPCR)/2
        elif dirLeft and not dirRight:
            # turning right
            self.move = 0
        elif not dirLeft and dirRight:
            # turning left
            self.move = 0
        elif not dirLeft and not dirRight:
            # reverse
            self.move = -(diffPCL+diffPCR)/2

        # Update position
        self.x += math.cos(self.angleSum/self.i) * self.move * (WHEEL_CIRCUMFERENCE / self.pulsesPerRotation)
        self.y += math.sin(self.angleSum/self.i) * self.move * (WHEEL_CIRCUMFERENCE / self.pulsesPerRotation)

        self.oldPCL = self.currentPulseCountLeft
        self.oldPCR = self.currentPulseCountRight


    RIGHT = -1  # turning clockwise (right)
    LEFT = 1  # turning counter clockwise (left)

    def accurate_turn(self, angle, direction=RIGHT):
        """
        Do an accurate turn.

        :param angle: Degrees to turn
        :param direction: Direction to turn (Driving.LEFT or Driving.RIGHT)
        """
        if direction == self.RIGHT:  # Set direction for Clockwise turn
            self.directionLeftWheel = False
            self.directionRightWheel = True
        elif direction == self.LEFT:  # Set direction for Counterclockwise turn
            self.directionLeftWheel = True
            self.directionRightWheel = False
        else:
            raise ValueError('Did not receive a correct direction.')

        wheel_travel_distance = ((WHEEL_BASE_CIRCUMFERENCE * angle) / 360)

        # the number of degrees each wheel needs to turn
        wheel_turn_degrees = ((wheel_travel_distance / WHEEL_CIRCUMFERENCE) * 360)
        degrees_per_pulse = 360 / ENCODER_PPR

        # Reset encoder counts and set new target counts
        self.encoderLeft.reset_pulse_count()
        self.encoderRight.reset_pulse_count()
        self.setpointPulseCountLeft = round(wheel_turn_degrees / degrees_per_pulse)
        self.setpointPulseCountRight = round(wheel_turn_degrees / degrees_per_pulse)

        print("LEFT  turning for %0.1f deg (%d pulses)" % (wheel_turn_degrees, self.setpointPulseCountLeft))
        print("RIGHT turning for %0.1f deg (%d pulses)" % (wheel_turn_degrees, self.setpointPulseCountRight))

        self.speed = self.rotationspeed  # set speed
        self.drive()

    def returnPosition(self):
        return self.x, self.y, self.orientation

    def initXY(self, x, y):
        # set initial values for x and y
        self.x = x
        self.y = y
        self.setpointX = x
        self.setpointY = y

    def changePWMLeft(self, amount):
        self.increaseLeftPWM += amount

    def changePWMRight(self, amount):
        self.increaseRightPWM += amount

    def resetOrientation(self):
        self.orientation = self.setpointOrientation

    def stop(self):
        self.rightPWM = 0
        self.leftPWM = 0
        self.speed = 0
        self.rotationspeed = 0


if __name__ == '__main__':
    try:
        motor = Driving()

        # Do some test driving
        motor.turnLeft(2)
        time.sleep(0.3)
        while True:
            motor.driveForward(5, 50)
            time.sleep(0.3)

    except KeyboardInterrupt:  # Exit by pressing CTRL + C
        print("Measurement stopped by User")
    finally:
        motor.leftPinF.stop()
        motor.leftPinB.stop()
        motor.rightPinF.stop()
        motor.rightPinB.stop()
        GPIO.cleanup()
