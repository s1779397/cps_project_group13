import time

import RPi.GPIO as GPIO

# Two types of import for two different versions of the Pi

# from mappingbot.constants import ULTRASOUND_ECHO_RIGHT, ULTRASOUND_TRIGGER_RIGHT, ULTRASOUND_ECHO_FORWARD, \
#     ULTRASOUND_TRIGGER_FORWARD, ULTRASOUND_ECHO_LEFT, ULTRASOUND_TRIGGER_LEFT
# from mappingbot.ultrasound import UltrasoundSensor, UltrasoundGroup
# from mappingbot.storage import DataStorage, DataCollector
# from mappingbot.driving import Driving

from constants import ULTRASOUND_ECHO_RIGHT, ULTRASOUND_TRIGGER_RIGHT, ULTRASOUND_ECHO_FORWARD, \
    ULTRASOUND_TRIGGER_FORWARD, ULTRASOUND_ECHO_LEFT, ULTRASOUND_TRIGGER_LEFT
from ultrasound import UltrasoundSensor, UltrasoundGroup
from storage import DataStorage, DataCollector
from driving import Driving
from virtual_mapping import StateMachine
from webserver import WebserverThread

if __name__ == '__main__':
    GPIO.setmode(GPIO.BCM)
    # Initialize overall data storage object
    data_storage = DataStorage()

    # Initialize all ultrasound sensors
    us_left = UltrasoundSensor(name='us_left', trigger_pin=ULTRASOUND_TRIGGER_LEFT,
                               echo_pin=ULTRASOUND_ECHO_LEFT)
    us_forward = UltrasoundSensor(name='us_forward', trigger_pin=ULTRASOUND_TRIGGER_FORWARD,
                                  echo_pin=ULTRASOUND_ECHO_FORWARD)
    us_right = UltrasoundSensor(name='us_right', trigger_pin=ULTRASOUND_TRIGGER_RIGHT,
                                echo_pin=ULTRASOUND_ECHO_RIGHT)
    us_group = UltrasoundGroup(sensors=[us_left, us_forward, us_right], storage=data_storage)

    # Initialize driving object
    driving = Driving()

    # Initialize state machine
    state_machine = StateMachine(data_storage=data_storage, driving_tracker=driving)

    # Initialize data collector
    data_collector = DataCollector(data_storage=data_storage, driving_tracker=driving, ultrasound_group=us_group, explore_tracker=state_machine.explore,
                                   interval=0.7)

    # Initialize the webserver thread
    webserver = WebserverThread(host='0.0.0.0', debug=False, explorer=state_machine.explore)
    webserver.start()

    try:
        state_machine.changeStateManual("followWall")
        us_group.start()
        data_collector.start()
        print('all started')

        while True:
            state_machine.run()

    except KeyboardInterrupt:
        print("INTERRUPTED")
        state_machine.explore.returnToBluetoothDevice()

    finally:
        driving.stop()
        # save to disk
        state_machine.explore.save_data_map()
        state_machine.explore.save_history_map()
        state_machine.explore.save_virtual_map()
        state_machine.explore.save_bluetooth_map()
        data_storage.save_to_disk()
        data_collector.interrupt()
        us_group.interrupt()
        GPIO.cleanup()
        print("DONE")
